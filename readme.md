# Petrolink front-end challenge

## Prerequisites
[Node.js and npm](https://nodejs.org/en/): LTS version.

[NPM](http://npmjs.org/) is bundled with node, but we need to update it:

```
npm i -g npm
```

## Running the server
You first need to fetch dependencies:

```
cd path/to/this/folder
npm install
```

You can then start the server:

```
npm start
```

This will: 
- serve any static files in the ```public``` folder
- start a websocket server that streams data points

on port 8080