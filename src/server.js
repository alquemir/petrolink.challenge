// Configuration
const PORT = 8080;
const MESSAGE_RATE = 5;  // Messages per second;
const TIMEOUT = 1000 / MESSAGE_RATE;

const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const fs = require('fs');
const readline = require('readline');
const path = require('path');

app.use(express.static(__dirname + '/../public'));

app.get('/ping', function (req, res) {
    res.send("pong ...");
});


io.on('connection', function (socket) {
    var intervalId = undefined;
    var totalLinesRead = 0;
    var csvFilePath = path.resolve(__dirname, "../data/Time_HKLD.csv");

    var buffer = [];

    var rl = readline.createInterface({
        input: fs.createReadStream(csvFilePath)
    });

    var resumeStream = function () {
        rl.resume();
        intervalId = setInterval(shift, TIMEOUT);
    };

    var pauseStream = function () {
        rl.pause();
        clearInterval(intervalId);
    };

    var shift = function () {
        if (rl.input.closed && !buffer.length) {
            return;
        }

        if (buffer.length) {
            socket.emit('data', buffer.shift());
        }
    };

    rl.on('line', function (line) {
        var arr = line.split(',');
        buffer.push({timestamp: arr[0], hkld: arr[1]});
        totalLinesRead++;
    });

    rl.on('close', function () {
        socket.emit("end of file ...")
    });

    socket.on('resume', function () {
        resumeStream();
    });

    socket.on('pause', function () {
        pauseStream();
    });

    socket.on('disconnect', function () {
        rl.close();
        buffer = [];
    });

});

server.listen(PORT, function () {
    console.log('Server started. Browse http://localhost:' + PORT);
});
