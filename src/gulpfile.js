const gulp = require('gulp');


const paths = {
    scripts: [
        '../node_modules/angular/angular.js',
        '../node_modules/moment/moment.js',
        '../node_modules/lodash/lodash.js'
    ],
    css: [
        '../node_modules/normalize.css/normalize.css',
        '../node_modules/bootstrap/dist/css/bootstrap.css'
    ],
    fonts: [
        '../node_modules/bootstrap/dist/fonts/**'
    ],
    dist: {
        js: '../public/app/vendor/js/',
        css: '../public/app/vendor/css/',
        fonts: '../public/app/vendor/fonts/'
    }
};

gulp.task('scripts', function () {
    return gulp.src(paths.scripts)
        .pipe(gulp.dest(paths.dist.js));
});

gulp.task('css', function () {
    return gulp.src(paths.css)
        .pipe(gulp.dest(paths.dist.css));
});

gulp.task('fonts', function () {
    return gulp.src(paths.fonts)
        .pipe(gulp.dest(paths.dist.fonts));
});

gulp.task('default', [
    'scripts',
    'css',
    'fonts'
]);