describe("DateTimeScale", function () {
    var DateTimeScale;

    beforeEach(angular.mock.module("app"));

    beforeEach(angular.mock.inject(function ($injector) {
        DateTimeScale = $injector.get("DateTimeScale");
    }));

    it("should calculate the correct date/time scale given two time intervals", function () {




        var maxDate = moment("2016-01-01T00:20:20", "YYYY-MM-DDTHH:mm:ss");
        var minDate = moment("2016-01-01T00:10:00", "YYYY-MM-DDTHH:mm:ss");

        var scale = new DateTimeScale({
            min: data[0],
            max: data[data.length - 1],
            minRange: 0,
            maxRange: 100
        });

        scale.init();

        var totalTicks = scale.calculateTotalMajorTicks();
        for(var i = 0; i < totalTicks; i++) {
            var mTickValue = scale.calculateMajorTickValue(i);
            var date = moment(mTickValue).format("YYYY-MM-DDTHH:mm:ss");
            console.log(date);
        }

        expect(1 == 1).toBeTruthy();

    });

});
