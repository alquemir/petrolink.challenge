describe("LinearScale", function () {
    var LinearScale;

    beforeEach(angular.mock.module("app"));

    beforeEach(angular.mock.inject(function ($injector) {
        LinearScale = $injector.get("LinearScale");
    }));

    it("provided a domain value it should return the correct range value", function() {
        var scale = new LinearScale({
            min: 10,
            max: 130,
            minRange: 0,
            maxRange: 960
        });

        expect(scale.getRangeValue(20)).toBe(80);
        expect(scale.getRangeValue(50)).toBe(320);

    });

    it("provided a range value it should return the correct corresponding domain value", function() {
        var scale = new LinearScale({
            min: 10,
            max: 130,
            minRange: 0,
            maxRange: 960
        });

        expect(scale.getDomainValue(80)).toBe(20);
        expect(scale.getDomainValue(320)).toBe(50);
    });

    it("should calculate the correct total of axis ticks", function () {
        var scale = new LinearScale({
            min: 10,
            max: 130,
            minRange: 0,
            maxRange: 960
        });

        scale.init();
        expect(scale.calculateTotalMajorTicks()).toBe(8);
    });

    it("should calculate the correct 'major tick spacing'", function () {
        var scale = new LinearScale({
            min: 10,
            max: 130,
            minRange: 0,
            maxRange: 960
        });

        scale.init();
        expect(scale.majorTickSpacing).toBe(20);
    });

    it("should calculate the correct 'minor tick spacing'", function () {
        var scale = new LinearScale({
            min: 10,
            max: 30,
            minRange: 0,
            maxRange: 960
        });

        scale.init();
        expect(scale.minorTickSpacing).toBe(5);
    });

    it("should calculate the correct number of major ticks", function () {
        var scale = new LinearScale({
            min: 10,
            max: 130,
            minRange: 0,
            maxRange: 960
        });

        scale.init();
        expect(scale.totalMajorTicks).toBe(7);
    });

    it("should calculate the correct number of minor ticks", function () {
        var scale = new LinearScale({
            min: 10,
            max: 130,
            minRange: 0,
            maxRange: 960
        });

        scale.init();
        expect(scale.totalMinorTicks).toBe(5);
    });

    it("test", function() {
        var scale = new LinearScale({
            min: 5,
            max: 35,
            minRange: 0,
            maxRange: 100
        });

        scale.init();

        var totalTicks = scale.calculateTotalMajorTicks();
        var fTick = scale.calculateFirstMajorTickValue();

        for(var i = 0; i < totalTicks; i++) {
            var mTickValue = scale.calculateMajorTickValue(i);
            console.log(mTickValue);
        }

    });

});
