describe("Matrix3", function() {
  var Matrix3;
  var Vector3;
  var Vector2;
  var classUnderTest;

  beforeEach(angular.mock.module("app"));

  beforeEach(angular.mock.inject(function ($injector) {
    Matrix3 = $injector.get("Matrix3");
    Vector3 = $injector.get("Vector3");
    Vector2 = $injector.get("Vector2");
  }));

  it("should calculate the identity matrix correctly", function() {
    expect(Matrix3.getIdentity()).toEqual(new Matrix3(
      1, 0, 0,
      0, 1, 0,
      0, 0, 1
    ));
  });

  it("should calculate the transpose matrix correctly", function() {
    var classUnderTest = new Matrix3(
      1, 2, 3,
      4, 5, 6,
      7, 8, 9
    );

    expect(classUnderTest.getTranspose()).toEqual(new Matrix3(
      1, 4, 7,
      2, 5, 8,
      3, 6, 9
    ));
  });

  it("should calculate the determinant correctly", function() {
    var classUnderTest = new Matrix3(
      1, 2, 3,
      4, 5, 6,
      7, 8, 9
    );

    expect(classUnderTest.getDeterminant()).toBe(0);
  });

  it("should calculate the adjugate correctly", function() {
    var classUnderTest = new Matrix3(
      -3, 2, -5,
      -1, 0, -2,
       3, -4, 1
    );

    expect(classUnderTest.getAdjugate()).toEqual(new Matrix3(
        -8, 18, -4,
        -5, 12, -1,
         4, -6 , 2
    ));
  });

  it("should calculate the adjugate correctly", function() {
    var classUnderTest = new Matrix3(
      1, 2, 3,
      0, 1, 4,
      5, 6, 0
    );

    expect(classUnderTest.getAdjugate()).toEqual(new Matrix3(
        -24, 18, 5,
        20, -15, -4,
        -5, 4, 1
    ));
  });


  it("should calculate the inverse correctly", function() {
    var classUnderTest = new Matrix3(
      1, 2, 3,
      0, 1, 4,
      5, 6, 0
    );

    expect(classUnderTest.getInverse()).toEqual(new Matrix3(
        -24, 18, 5,
        20, -15, -4,
        -5, 4, 1
    ));
  });

  it("should calculate the matrix multiplication correctly", function() {
      var classUnderTest = new Matrix3(
         1,2,3,
         3,2,1,
         2,1,3
      );

      expect(classUnderTest.multiplyMatrix3(new Matrix3(
          4,5,6,
          6,5,4,
          4,6,5
      ))).toEqual(new Matrix3(
        28, 33, 29,
        28, 31, 31,
        26, 33, 31
      ));
  });

  it("should calculate the vector3 multiplication correctly", function() {
    var classUnderTest = new Matrix3(
      2, 1, 2,
      3, 2, 3,
      4, 1, 1
    );

    expect(classUnderTest.multiplyVector3(new Vector3(1, 2, 3))).toEqual(new Vector3(10, 16, 9));
  });

  it("should calculate the vector2 multiplication correctly", function() {
    var classUnderTest = new Matrix3(
      2, 1, 2,
      3, 2, 3,
      0, 0, 1
    );

    expect(classUnderTest.multiplyVector2(new Vector2(1, 2))).toEqual(new Vector2(6, 10));
  });

  it("should calculate the NDC space correctly", function() {
    var classUnderTest = Matrix3.getWorldToNdcSpaceMatrix(-400, 400, -300, 300);

    expect(classUnderTest.multiplyVector2(new Vector2(400, 300))).toEqual(new Vector2(1, 1));
    expect(classUnderTest.multiplyVector2(new Vector2(-400, -300))).toEqual(new Vector2(-1, -1));

  });

  it("should calculate the Viewport Space correctly", function() {
    var classUnderTest = Matrix3.getNdcToScreenSpaceMatrix(0, 800, 0, 600);

    expect(classUnderTest.multiplyVector2(new Vector2(1, 1))).toEqual(new Vector2(800, 600));
    expect(classUnderTest.multiplyVector2(new Vector2(-1, -1))).toEqual(new Vector2(0, 0));
  });


});
