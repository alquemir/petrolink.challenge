describe("Vector2", function() {
    var Vector2;
    var classUnderTest;

    beforeEach(angular.mock.module("app"));

    beforeEach(angular.mock.inject(function($injector) {
        Vector2 = $injector.get('Vector2');
    }));

    it("should calculate magnitude correctly", function() {
        classUnderTest = new Vector2(6, 8);
        expect(classUnderTest.mag()).toBe(10);
    });

    it("should calculate the dot product correctly", function() {
        classUnderTest = new Vector2(1, 2);
        expect(classUnderTest.dot(new Vector2(3, 4))).toBe(11);
    });

    it("should calculate the cross product correctly", function() {
        classUnderTest = new Vector2(1, 2);
        expect(classUnderTest.cross(new Vector2(3, 4))).toBe(-2);
    });

    it("should calculate the euclidean distance correctly", function () {
        classUnderTest = new Vector2(-2, 1);
        expect(classUnderTest.distanceTo(new Vector2(1, 5))).toBe(5);
    });


    it("should calculate the correct distance from the point to the line segment", function () {
        var p0 = new Vector2(0, 1);
        var p1 = new Vector2(1, 4);
        var q = new Vector2(1, 2);

        var d = q.distanceToLineSegment([p0, p1]);
        console.log(d);
    });
    /*
    it("should normaise the vector correctly", function() {
      classUnderTest = new Vector2(6, 8);
      expect(classUnderTest.normalise()).toEqual(new Vector2(3/5, 4/5))
    });
    */
});
