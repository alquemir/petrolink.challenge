describe("Vector3", function() {
    var Vector3;
    var classUnderTest;

    beforeEach(angular.mock.module("app"));

    beforeEach(angular.mock.inject(function($injector) {
        Vector3 = $injector.get('Vector3');
    }));


    it("should calculate magnitude correctly", function() {
        classUnderTest = new Vector3(3, 2, -1);
        expect(classUnderTest.mag()).toBe(Math.sqrt(14));
    });

    it("should calculate the dot product correctly", function() {
        classUnderTest = new Vector3(3, -2, 7);
        expect(classUnderTest.dot(new Vector3(0, 4, -1))).toBe(-15);
    });

    it("should calculate the cross product correctly", function() {
        classUnderTest = new Vector3(1, 3, 4);
        expect(classUnderTest.cross(new Vector3(2, -5, 8))).toEqual(new Vector3(44, -0, -11));
    });

});
