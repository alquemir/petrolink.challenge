describe("KdTree", function() {
    var KdTree;
    var KdTree2;
    var HyperPoint;

    beforeEach(angular.mock.module("app"));

    beforeEach(angular.mock.inject(function ($injector) {
        KdTree = $injector.get("KdTree");
        KdTree2 = $injector.get("KdTree2");
        HyperPoint = $injector.get("HyperPoint");
    }));

    it("test", function () {
        /*
        var coordinates = [
            [2, 3, 3], [5, 4, 2], [9, 6, 7], [4, 7, 9], [8, 1, 5],
            [7, 2, 6], [9, 4, 1], [8, 4, 2], [9, 7, 8], [6, 3, 1],
            [3, 4, 5], [1, 6, 8], [9, 5, 3], [2, 1, 3], [8, 7, 6],
            [5, 4, 2], [6, 3, 1], [8, 7, 6], [9, 6, 7], [2, 1, 3],
            [7, 2, 6], [4, 7, 9], [1, 6, 8], [3, 4, 5], [9, 4, 1]];


            var points = [[2, 2], [1, 1], [3, 3]];

            var tree = new KdTree();
            tree.insertPoints(points, 'key2');

            console.log(tree.getPoints(0, true, 'key2'));

         */
        var points2 = [
            [5, 5],
            [5, 5],
            [1, 3],
            [2, 2],
            [2, 2],
            [4, 4]
        ];

        var tree2 = new KdTree2();
        tree2.createKdTree(points2);

        var points = [
            new HyperPoint([4, 5], "key-1"),
            new HyperPoint([5, 5], "key-1"),
            new HyperPoint([1, 3], "key-1"),
            new HyperPoint([2, 2], "key-1"),
            new HyperPoint([2, 2], "key-1"),
            new HyperPoint([4, 4], "key-1")
        ];

        var tree = new KdTree();
        tree.createKdTree(points);

        var minPoint = tree.findMinimumPoint();
        console.log(minPoint);

        var maxPoint = tree.findMaximumPoint();
        console.log(maxPoint);

        console.log(tree.getPoints());
    })
});
