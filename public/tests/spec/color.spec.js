describe("Color", function () {
    var Color;

    beforeEach(angular.mock.module("app"));

    beforeEach(angular.mock.inject(function ($injector) {
        Color = $injector.get("Color");
    }));

    it("should convert to HEX values", function () {
        expect((new Color(255, 255, 0)).toHex()).toBe("#FFFF00");
        expect((new Color(125, 24, 255)).toHex()).toBe("#7D18FF");
        expect((new Color(255, 255, 255)).toHex()).toBe("#FFFFFF");

        var r = new Color(0, 0, 0);
        var v = r.toHex();
        expect(v).toBe("#000000");
    });

});
