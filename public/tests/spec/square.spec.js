describe("Square", function () {
  var Square;
  var Color;
  var Vector2;

  beforeEach(angular.mock.module("app"));

  beforeEach(angular.mock.inject(function ($injector) {
    Square = $injector.get("Square");
    Color = $injector.get("Color");
    Vector2 = $injector.get("Vector2");
  }));

  it("should translate the square", function() {
    var classUnderTest = new Square("Square1", 20, new Color(255, 0, 0));
    classUnderTest.translate(10, 10);
    expect(classUnderTest.points).toEqual([
      new Vector2(0, 20),
      new Vector2(20, 20),
      new Vector2(20, 0),
      new Vector2(0, 0)
    ]);
  });

  it("should scale the square", function() {
    var classUnderTest = new Square("Square1", 20, new Color(255, 0, 0));
    classUnderTest.scale(2, 2);
    expect(classUnderTest.points).toEqual([
      new Vector2(-20, 20),
      new Vector2(20, 20),
      new Vector2(20, -20),
      new Vector2(-20, -20)
    ]);
  });

});
