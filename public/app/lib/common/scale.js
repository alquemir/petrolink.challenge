(function (window, angular) {
    var Scale = function (config) {
        this.config = angular.extend({}, config);
        this.min = this.config.min || 0.0;
        this.max = this.config.max || 1.0;
        this.minRange = this.config.minRange || 0.0;
        this.maxRange = this.config.maxRange || 1.0;
        this.minorTickSpacing = this.config.minorTickSpacing || 0.1;
        this.majorTickSpacing = this.config.majorTickSpacing || 0.1;
        this.totalMajorTicks = this.config.totalMajorTicks || 7.0;
        this.totalMinorTicks = this.config.totalMinorTicks || 5.0;
        this.enableAutoMajorTick = this.config.enableAutoMajorTick || true;
        this.enableAutoMinorTick = this.config.enableAutoMinorTick || true;
    };

    Scale.prototype.setDomain = function (min, max) {
        this.min = min || 0;
        this.max = max || 1;
    };


    /// Gets the corresponding range value relative to an origin that has been set up
    /// on the left
    Scale.prototype.getLocalRangeValue = function (x) {
        var ratio = (x - this.min) / (this.max - this.min);
        return (this.maxRange - this.minRange) * (1.0 - ratio);
    };

    Scale.prototype.calculateNumberOfFractionDigits = function (d) {
        return Math.max(-Math.floor(Math.log(d) / Math.LN10), 0);
    };

    Scale.prototype.setRange = function (minRange, maxRange) {
        this.minRange = minRange || 0;
        this.maxRange = maxRange || 1;
    };

    /// Uses max-min normalization
    /// Gets the corresponding domain value
    Scale.prototype.getDomainValue = function (x) {
        return ((x - this.minRange) / (this.maxRange - this.minRange)) * (this.max - this.min) + this.min;
    };

    /// Gets the corresponding range value
    Scale.prototype.getRangeValue = function (x) {
        return ((x - this.min) / (this.max - this.min)) * (this.maxRange - this.minRange) + this.minRange;
    };

    Scale.prototype.calculateDomainMinimumValue = function () {
        return Math.floor(this.min / this.majorTickSpacing) * this.majorTickSpacing;
    };

    Scale.prototype.calculateDomainMaximumValue = function () {
        return Math.ceil(this.max / this.majorTickSpacing) * this.majorTickSpacing;
    };

    /// Determines the total number of major ticks
    Scale.prototype.calculateTotalMajorTicks = function () {
        var totalTicks = Math.floor((this.max - this.min) / this.majorTickSpacing) + 1;

        if ( totalTicks < 1 ) totalTicks = 1;
        if ( totalTicks > 1000 ) totalTicks = 1000;

        return totalTicks;
    };

    /// Determine the value for any major tick.
    Scale.prototype.calculateMajorTickValue = function (firstMajorTick, tickValue) {
        var fractionDigits = this.calculateNumberOfFractionDigits(this.majorTickSpacing);
        var value = firstMajorTick + this.majorTickSpacing * tickValue;

        if ( value < this.min ) value = this.min;
        if ( value > this.max ) value = this.max;

        return value % 1 != 0 ? value.toFixed(fractionDigits) : value;
    };

    /// Determine the value for the first major tick.
    Scale.prototype.calculateFirstMajorTickValue = function () {
        return Math.ceil(this.min / this.majorTickSpacing) * this.majorTickSpacing;
    };

    /// Internal routine to determine the ordinals of the first minor tick mark
    Scale.prototype.calculateFirstMinorTickValue = function (firstMajorTickValue) {
        return Math.floor(this.min - firstMajorTickValue) / this.minorTickSpacing;
    };

    /// Determine the value for any minor tick.
    Scale.prototype.calculateMinorTickValue = function (firstMajorTickValue, minorTickValue) {
        return firstMajorTickValue + this.minorTickSpacing * minorTickValue;
    };


    /// Calculates the tick spacing
    Scale.prototype.calculateTickSpacing = function (min, max, totalTicks) {
        var range = this.niceNum(max - min, false);
        return this.niceNum(range / (totalTicks - 1), true);
    };

    //Taken from http://stackoverflow.com/questions/8506881/nice-label-algorithm-for-charts-with-minimum-ticks
    Scale.prototype.niceNum = function (range, round) {
        var exponent = Math.floor(Math.log(range) / Math.LN10);
        var fraction = range / Math.pow(10, exponent);

        var niceFraction = 0.0;
        if ( round ) {
            if ( fraction < 1.5 ) {
                niceFraction = 1;
            }
            else if ( fraction < 3 ) {
                niceFraction = 2;
            }
            else if ( fraction < 7 ) {
                niceFraction = 5;
            }
            else {
                niceFraction = 10;
            }
        }
        else {
            if ( fraction <= 1 ) {
                niceFraction = 1;
            }
            else if ( fraction <= 2 ) {
                niceFraction = 2;
            }
            else if ( fraction <= 5 ) {
                niceFraction = 5;
            }
            else {
                niceFraction = 10;
            }
        }

        return niceFraction * Math.pow(10, exponent);
    };

    angular.module("app").factory("Scale", function () {
        return Scale;
    });

})(
    window,
    window.angular
);
