(function (window, angular, Component, Vector2, Color) {

    /**
     * Creates a square of equal length
     * @param config - square's configuration
     */
    var Square = function (config) {
        this.config = angular.extend({}, config);
        this.renderer = this.config.renderer;
        this.id = this.config.id || "";
        this.size = this.config.size || 0;
        this.color = this.config.color || Color.RED;
        this.isFill = this.config.isFill || false;
        this.isStroke = this.config.isStroke || true;
        this.lineWidth = this.config.lineWidth || 0.5;
        this.clipRect = this.config.clipRect || undefined;

        Component.call(this, this.id, this.renderer);
    };

    Square.prototype = Object.create(Component.prototype);
    Square.prototype.constructor = Square;

    Square.prototype.getPoints = function () {
        return [
            new Vector2(-this.size / 2, this.size / 2),
            new Vector2(-this.size / 2, -this.size / 2),
            new Vector2(this.size / 2, -this.size / 2),
            new Vector2(this.size / 2, this.size / 2)
        ];
    };

    Square.prototype.clipPoints = function (points) {
        if (!this.clipRect) return points;
        return this.clipRect.clipPolygon(points);
    };

    Square.prototype.render = function () {

        var tPoints = this.transformation.apply(this.getPoints());
        var cPoints = this.clipPoints(tPoints);

        if (cPoints.length == 0) return;

        var pPoints = this.renderer.camera.projectWorldToViewportSpacePoints(cPoints);
        this.renderer.renderingContext.plotPath(pPoints, this.color, this.isFill, this.isStroke, this.lineWidth);
    };

    angular.module("app").factory("Square", function () {
        return Square;
    });
})(
    window,
    window.angular,
    window.angular.injector(['ng', 'app']).get('Component'),
    window.angular.injector(['ng', 'app']).get('Vector2'),
    window.angular.injector(['ng', 'app']).get('Color')
);
