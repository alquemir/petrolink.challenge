(function (window, angular, Vector2, Vector3) {
    var Matrix3 = function (m11, m12, m13, m21, m22, m23, m31, m32, m33) {
        this.m11 = m11 || 0;
        this.m12 = m12 || 0;
        this.m13 = m13 || 0;
        this.m21 = m21 || 0;
        this.m22 = m22 || 0;
        this.m23 = m23 || 0;
        this.m31 = m31 || 0;
        this.m32 = m32 || 0;
        this.m33 = m33 || 0;
    };

    /**
     * Get identity matrix
     * @return {Matrix3}
     */
    Matrix3.getIdentity = function () {
        return new Matrix3(
            1, 0, 0,
            0, 1, 0,
            0, 0, 1
        );
    };

    Matrix3.prototype.getTranspose = function () {
        return new Matrix3(
            this.m11, this.m21, this.m31,
            this.m12, this.m22, this.m32,
            this.m13, this.m23, this.m33
        );
    };

    Matrix3.prototype.getDeterminant = function () {
        return this.m11 * this.m22 * this.m33
            + this.m12 * this.m23 * this.m31
            + this.m13 * this.m21 * this.m32
            - this.m13 * this.m22 * this.m31
            - this.m12 * this.m21 * this.m33
            - this.m11 * this.m23 * this.m32;
    };

    Matrix3.prototype.getAdjugate = function () {
        var result = new Matrix3();

        result.m11 = ( this.m22 * this.m33 - this.m32 * this.m23 );
        result.m12 = -( this.m12 * this.m33 - this.m32 * this.m13 );
        result.m13 = ( this.m12 * this.m23 - this.m22 * this.m13 );

        result.m21 = -( this.m21 * this.m33 - this.m31 * this.m23 );
        result.m22 = ( this.m11 * this.m33 - this.m31 * this.m13 );
        result.m23 = -( this.m11 * this.m23 - this.m21 * this.m13 );

        result.m31 = ( this.m21 * this.m32 - this.m31 * this.m22 );
        result.m32 = -( this.m11 * this.m32 - this.m31 * this.m12 );
        result.m33 = ( this.m11 * this.m22 - this.m21 * this.m12 );

        return result;
    };

    Matrix3.prototype.getInverse = function () {

        var result = new Matrix3();
        var det = this.getDeterminant();

        if (det === 0) {
            return this.getIdentity();
        }

        var invDet = 1.0 / det;
        var adjugateMatrix = this.getAdjugate();

        result.m11 = invDet * ( adjugateMatrix.m11 );
        result.m12 = invDet * ( adjugateMatrix.m12 );
        result.m13 = invDet * ( adjugateMatrix.m13 );

        result.m21 = invDet * ( adjugateMatrix.m21 );
        result.m22 = invDet * ( adjugateMatrix.m22 );
        result.m23 = invDet * ( adjugateMatrix.m23 );

        result.m31 = invDet * ( adjugateMatrix.m31 );
        result.m32 = invDet * ( adjugateMatrix.m32 );
        result.m33 = invDet * ( adjugateMatrix.m33 );

        return result;
    };

    /**
     * Multiply by a 3x3 Matrix
     * @param  {Matrix3} mat Matrix to be multiplied by
     * @return {Matrix3}
     */
    Matrix3.prototype.multiplyMatrix3 = function (mat) {
        var result = new Matrix3();

        result.m11 = this.m11 * mat.m11 + this.m12 * mat.m21 + this.m13 * mat.m31;
        result.m12 = this.m11 * mat.m12 + this.m12 * mat.m22 + this.m13 * mat.m32;
        result.m13 = this.m11 * mat.m13 + this.m12 * mat.m23 + this.m13 * mat.m33;

        result.m21 = this.m21 * mat.m11 + this.m22 * mat.m21 + this.m23 * mat.m31;
        result.m22 = this.m21 * mat.m12 + this.m22 * mat.m22 + this.m23 * mat.m32;
        result.m23 = this.m21 * mat.m13 + this.m22 * mat.m23 + this.m23 * mat.m33;

        result.m31 = this.m31 * mat.m11 + this.m32 * mat.m21 + this.m33 * mat.m31;
        result.m32 = this.m31 * mat.m12 + this.m32 * mat.m22 + this.m33 * mat.m32;
        result.m33 = this.m31 * mat.m13 + this.m32 * mat.m23 + this.m33 * mat.m33;

        return result;
    };

    Matrix3.prototype.multiplyVector2 = function (vec) {
        var result = this.multiplyVector3(vec.toHomogeneousCoordinates());
        return new Vector2(
            result.x / result.z,
            result.y / result.z
        );
    };

    Matrix3.prototype.multiplyVector3 = function (vec) {
        var result = new Vector3();

        result.x = this.m11 * vec.x + this.m12 * vec.y + this.m13 * vec.z;
        result.y = this.m21 * vec.x + this.m22 * vec.y + this.m23 * vec.z;
        result.z = this.m31 * vec.x + this.m32 * vec.y + this.m33 * vec.z;

        return result;
    };

    /**
     * World to Normalised Screen Space transformation matrix
     * @param  {Number} xMin [description]
     * @param  {Number} xMax [description]
     * @param  {Number} yMin [description]
     * @param  {Number} yMax [description]
     * @return {Matrix3}     [description]
     */
    Matrix3.getWorldToNdcSpaceMatrix = function (xMin, xMax, yMin, yMax) {
        var result = new Matrix3();

        result.m11 = 2 / (xMax - xMin);
        result.m12 = 0;
        result.m13 = -((xMax + xMin) / (xMax - xMin));

        result.m21 = 0;
        result.m22 = (2 / (yMax - yMin));
        result.m23 = -((yMax + yMin) / (yMax - yMin));

        result.m31 = 0;
        result.m32 = 0;
        result.m33 = 1;

        return result;
    };

    /**
     * Normalised Space to Screen Space transformation matrix
     * @param  {Number} xMin [description]
     * @param  {Number} xMax [description]
     * @param  {Number} yMin [description]
     * @param  {Number} yMax [description]
     * @return {Matrix3}      [description]
     */
    Matrix3.getNdcToScreenSpaceMatrix = function (xMin, xMax, yMin, yMax) {
        var result = new Matrix3();

        result.m11 = (xMax - xMin) / 2;
        result.m12 = 0;
        result.m13 = (xMax + xMin) / 2;

        result.m21 = 0;
        result.m22 = -((yMax - yMin) / 2);
        result.m23 = (yMax + yMin) / 2;

        result.m31 = 0;
        result.m32 = 0;
        result.m33 = 1;

        return result;
    };

    /**
     * Translation matrix
     * @param  {Number} xT x-axis translation amount
     * @param  {Number} yT y-axis translation amount
     * @return {Matrix3} Translation matrix
     */
    Matrix3.getTranslationMatrix = function (xT, yT) {
        var result = new Matrix3();

        result.m11 = 1;
        result.m12 = 0;
        result.m13 = xT;
        result.m21 = 0;
        result.m22 = 1;
        result.m23 = yT;
        result.m31 = 0;
        result.m32 = 0;
        result.m33 = 1;

        return result;
    };

    /**
     * Scale matrix
     * @param  {Number} xS x-axis scale amount
     * @param  {Number} yS y-axis scale amount
     * @return {Matrix3} Scale matrix
     */
    Matrix3.getScaleMatrix = function (xS, yS) {
        var result = new Matrix3();

        result.m11 = xS;
        result.m12 = 0;
        result.m13 = 0;
        result.m21 = 0;
        result.m22 = yS;
        result.m23 = 0;
        result.m31 = 0;
        result.m32 = 0;
        result.m33 = 1;

        return result;
    };

    angular.module("app").factory('Matrix3', function () {
        return Matrix3;
    });
})(
    window,
    window.angular,
    angular.injector(['ng', 'app']).get('Vector2'),
    angular.injector(['ng', 'app']).get('Vector3')
);
