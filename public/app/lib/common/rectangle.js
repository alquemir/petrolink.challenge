(function (window, angular, Component, Vector2, Color) {

    var Rectangle = function (config) {
        this.config = angular.extend({}, config);

        this.id = this.config.id || "";
        this.left = this.config.left || 0;
        this.right = this.config.right || 0;
        this.top = this.config.top || 0;
        this.bottom = this.config.bottom || 0;
        this.isFill = this.config.isFill || false;
        this.isStroke = this.config.isStroke || true;
        this.color = this.config.color || Color.RED;
        this.lineWidth = this.config.lineWidth || 0.5;

        Component.call(this, this.id, this.config.renderer);
    };

    Rectangle.prototype = Object.create(Component.prototype);
    Rectangle.prototype.constructor = Rectangle;

    // https://en.wikipedia.org/wiki/Cohen%E2%80%93Sutherland_algorithm
    // To clip the all the lines that fall ouside the boundaries of the rectangle (chart rect, viewport rect etc..)
    Rectangle.OUTCODE = {
        INSIDE: 0, // 0000
        LEFT: 1,   // 0001
        RIGHT: 2,  // 0010
        BOTTOM: 4, // 0100
        TOP: 8     // 1000
    };




    Rectangle.prototype.computeOutCode = function (x, y) {
        var code = Rectangle.OUTCODE.INSIDE;  // initialised as being inside of [[clip window]]

        if (x < this.left) {    // to the left of clip window
            code |= Rectangle.OUTCODE.LEFT;
        }
        else if (x > this.right) {   // to the right of clip window
            code |= Rectangle.OUTCODE.RIGHT;
        }

        if (y < this.bottom) { // below the clip window
            code |= Rectangle.OUTCODE.BOTTOM;
        }
        else if (y > this.top) {     // above the clip window
            code |= Rectangle.OUTCODE.TOP;
        }

        return code;
    };


    /**
     * Cohen–Sutherland clipping algorithm clips a line from
     * P0 = (x0, y0) to P1 = (x1, y1) against a rectangle with
     * diagonal from (xmin, ymin) to (xmax, ymax), (left, bottom) to (top, right)
     * in our case
     */
    Rectangle.prototype.clipLine = function (p0, p1) {

        var x0 = p0.x;
        var y0 = p0.y;
        var x1 = p1.x;
        var y1 = p1.y;

        var outcode0 = this.computeOutCode(x0, y0);
        var outcode1 = this.computeOutCode(x1, y1);
        var accept = false;

        for (; ;) {

            if ((outcode0 | outcode1) == 0) { // Bitwise OR is 0. Trivially accept and get out of loop
                accept = true;
                break;
            }
            else if ((outcode0 & outcode1) != 0) { // Bitwise AND is not 0. Trivially reject and get out of loop
                break;
            }
            else {
                // failed both tests, so calculate the line segment to clip
                // from an outside point to an intersection with clip edge
                var x, y;

                // Pick at least one point outside rectangle
                var outcodeOut = outcode0 ? outcode0 : outcode1;

                // Now find the intersection point;
                // use formulas y = y0 + slope * (x - x0), x = x0 + (1 / slope) * (y - y0)
                if ((outcodeOut & Rectangle.OUTCODE.TOP) != 0) {            // point is above the clip rectangle
                    x = x0 + (x1 - x0) * (this.top - y0) / (y1 - y0);
                    y = this.top;
                }
                else if ((outcodeOut & Rectangle.OUTCODE.BOTTOM) != 0) {  // point is below the clip rectangle
                    x = x0 + (x1 - x0) * (this.bottom - y0) / (y1 - y0);
                    y = this.bottom;
                }
                else if ((outcodeOut & Rectangle.OUTCODE.RIGHT) != 0) {   // point is to the right of clip rectangle
                    y = y0 + (y1 - y0) * (this.right - x0) / (x1 - x0);
                    x = this.right;
                }
                else if ((outcodeOut & Rectangle.OUTCODE.LEFT) != 0) {     // point is to the left of clip rectangle
                    y = y0 + (y1 - y0) * (this.left - x0) / (x1 - x0);
                    x = this.left;
                }

                // Now we move outside point to intersection point to clip
                // and get ready for next pass.
                if (outcodeOut == outcode0) {
                    x0 = x;
                    y0 = y;
                    outcode0 = this.computeOutCode(x0, y0);
                }
                else {
                    x1 = x;
                    y1 = y;
                    outcode1 = this.computeOutCode(x1, y1);
                }
            }
        }

        if (accept) {
            return [
                new Vector2(x0, y0),
                new Vector2(x1, y1)
            ];
        }

        return null;

    };

    Rectangle.EDGE = {
        LEFT: 0,
        BOTTOM: 1,
        RIGHT: 2,
        TOP: 3
    };

    /**
     * The Sutherland-Hodgman clipping algorithm.
     * @param vertexArray
     */
    Rectangle.prototype.clipPolygon = function (vertexArray) {
        var tmpOutVertexArray;

        tmpOutVertexArray = this.clipEdge(vertexArray,  Rectangle.EDGE.LEFT);
        tmpOutVertexArray = this.clipEdge(tmpOutVertexArray,  Rectangle.EDGE.BOTTOM);
        tmpOutVertexArray = this.clipEdge(tmpOutVertexArray,  Rectangle.EDGE.RIGHT);
        tmpOutVertexArray = this.clipEdge(tmpOutVertexArray,  Rectangle.EDGE.TOP);

        return tmpOutVertexArray;
    };

    Rectangle.prototype.clipEdge = function (inVertexArray, clipBoundary) {
        var i, s, p;
        var outVertexArray = [];

        s = inVertexArray[inVertexArray.length - 1];

        for (var j = 0; j < inVertexArray.length; j++) {
            p = inVertexArray[j];
            if (this.inside(p, clipBoundary)) { // Cases 1 and 4 (s) -> (p)
                if (this.inside(s, clipBoundary)) {  // Case 1 : Wholly inside visible region - save endpoint (p)
                    outVertexArray.push(p);
                } else { //Case 4 : Enter visible region, outside to inside - save intersection (i) and endpoint (p)
                    i = this.intersect(s, p, clipBoundary);
                    outVertexArray.push(i);
                    outVertexArray.push(p);
                }
            } else { // Cases 2 and 3
                if(this.inside(s, clipBoundary)) { // Case 2 : Exit visible region, inside to outside - save the intersection (i)
                    i = this.intersect(s, p, clipBoundary);
                    outVertexArray.push(i);
                }

                //Case 3: Out of the visible region - do nothing in this case
            }

            s = p; //Advance to next pair of vertices
        }

        return outVertexArray;
    };

    Rectangle.prototype.intersect = function (p1, p2, clipBoundary) {
        var iPt = new Vector2();
        var slope;


        switch (clipBoundary) {
            case Rectangle.EDGE.LEFT:
                slope = (p2.y - p1.y) / (p2.x - p1.x);
                iPt.x = this.left;
                iPt.y = p1.y + (this.left - p1.x) * slope;
                break;
            case Rectangle.EDGE.RIGHT:
                slope = (p2.y - p1.y) / (p2.x - p1.x);
                iPt.x = this.right;
                iPt.y = p1.y + (this.right - p1.x) * slope;
                break;
            case Rectangle.EDGE.TOP:
                slope = (p2.x - p1.x) / (p2.y - p1.y);
                iPt.x = p1.x + (this.top - p1.y) * slope;
                iPt.y = this.top;
                break;
            case Rectangle.EDGE.BOTTOM:
                slope = (p2.x - p1.x) / (p2.y - p1.y);
                iPt.x = p1.x + (this.bottom - p1.y) * slope;
                iPt.y = this.bottom;
                break;
        }



        return iPt;
    };

    Rectangle.prototype.inside = function (vertex, clipBoundary) {

        switch (clipBoundary) {
            case Rectangle.EDGE.LEFT:
                return vertex.x >= this.left;
            case Rectangle.EDGE.RIGHT:
                return vertex.x <= this.right;
            case Rectangle.EDGE.TOP:
                return vertex.y <= this.top;
            case Rectangle.EDGE.BOTTOM:
                return vertex.y >= this.bottom;
        }

    };


    Rectangle.prototype.getHeight = function () {
        return this.top - this.bottom;
    };

    Rectangle.prototype.getWidth = function () {
        return this.right - this.left;
    };

    Rectangle.prototype.getPoints = function () {
        return [
            new Vector2(this.left, this.top),
            new Vector2(this.right, this.top),
            new Vector2(this.right, this.bottom),
            new Vector2(this.left, this.bottom)
        ];
    };

    /**
     * Returns true if this rectangle contain the point.
     * Returns if this rectangle contain the point,
     * possibly at the boundary; false otherwise
     */
    Rectangle.prototype.contains = function (point) {
        return (point.x >= this.left) && (point.x <= this.right)
            && (point.y >= this.bottom) && (point.y <= this.top);
    };

    // Returns true if this rectangle intersects another.
    Rectangle.prototype.intersects = function (rect) {
        return this.right >= rect.left && this.top >= rect.bottom
            && rect.right >= this.left && rect.top >= this.bottom;
    };

    /**
     * Returns the Euclidean distance between this rectangle and the point {p}.
     * Restuns the Euclidean distance between the point {p} and the closest point
     * on this rectangle; 0 if the point is contained in this rectangle
     */
    Rectangle.prototype.distanceTo = function (point) {
        return Math.sqrt(this.distanceSquareTo(point));
    };

    /**
     * Returns the square of the Euclidean distance between this rectangle and the point {@code p}.
     * Returns the square of the Euclidean distance between the point {@code p} and
     * the closest point on this rectangle; 0 if the point is contained
     * in this rectangle
     */
    Rectangle.prototype.distanceSquareTo = function (point) {
        var dx = 0, dy = 0;

        if (point.x < this.left) dx = point.x - this.left;
        else if (point.x > this.right) dx = point.x - this.right;
        if (point.y < this.bottom) dy = point.y - this.bottom;
        else if (point.y > this.top) dy = point.y - this.top;

        return dx * dx + dy * dy;
    };


    /**
     * Function responsible for rendering the rectangle in drawing device
     *
     */
    Rectangle.prototype.render = function () {
        var points = this.renderer.camera.projectWorldToViewportSpacePoints(this.getPoints());
        this.renderer.renderingContext.plotPath(points, this.color, this.isFill, this.isStroke, this.lineWidth)
    };

    angular.module("app").factory('Rectangle', function () {
        return Rectangle;
    });

})(
    window,
    window.angular,
    window.angular.injector(['ng', 'app']).get('Component'),
    window.angular.injector(['ng', 'app']).get('Vector2'),
    window.angular.injector(['ng', 'app']).get('Color')
);
