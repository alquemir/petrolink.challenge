(function (window, angular, $log, HyperRectangle, HyperPoint) {


    var KdNode = function (point) {
        this.point = new HyperPoint(point || []);
        this.left = null;
        this.right = null;
    };

    var KdTree = function () {
        this.root = null;                   // Tree root node
        this.size = 0;                      // Total nodes in the tree
    };


    /**
     * Creates a multi-dimensional array
     * @param m - number of rows
     * @param n - number oi columns
     * @param initial
     * @returns {Array}
     */
    KdTree.prototype.createMultiArray = function (m, n, initial) {
        var a, mat = [];

        for (var i = 0; i < m; i++) {
            a = [];

            for (var j = 0; j < n; j++) {
                a[j] = initial;
            }

            mat[i] = a;
        }

        return mat;

    };


    /**
     * <p>
     * The {@code initializeReference} method initializes one reference
     * array.
     * </p>
     *
     * @param coordinates - an array of (x,y,z,w...) coordinates
     * @param reference - an array of references to the (x,y,z,w...) coordinates
     * key
     */
    KdTree.prototype.initializeReferences = function (coordinates, reference) {
        for (var i = 0; i < reference.length; i++) {
            reference[i] = coordinates[i];
        }
    };

    /**
     * <p>
     * The {@code superKeyCompare} method compares two int[] in as few
     * coordinates as possible and uses the sorting or partition coordinate
     * as the most significant coordinate.
     * </p>
     *
     * @param a - a int[]
     * @param b - a int[]
     * @param p - the most significant dimension
     * @returns number int that represents the result of comparing two super
     * keys
     */
    KdTree.prototype.superKeyCompare = function (a, b, p) {
        var diff = 0;

        for (var i = 0; i < a.length; i++) {
            // A fast alternative to the modulus operator for (i + p) < 2 * a.length.
            var r = (i + p < a.length) ? i + p : i + p - a.length;
            diff = a[r] - b[r];
            if (diff != 0) {
                break;
            }
        }
        return diff;
    };

    /**
     * <p>
     * The {@code mergeSort} function recursively subdivides the array to be
     * sorted then merges the elements. Adapted from Robert Sedgewick's
     * "Algorithms in C++" p. 166. Addison-Wesley, Reading, MA, 1992.
     * </p>
     *
     * @param reference - an array of references to the (x,y,z,w...)
     * coordinates
     * @param temporary - a scratch array into which to copy references;
     * this array must be as large as the reference array
     * @param low - the start index of the region of reference to sort
     * @param high - the high index of the region of reference to sort
     * @param index - the sorting partition (x, y, z, w...)
     */
    KdTree.prototype.mergeSort = function (reference, temporary, low, high, index) {
        var i, j, k;


        if (high > low) {

            // Avoid overflow when calculating the median address.
            var mid = low + ((high - low) >> 1);

            // Recursively subdivide the lower half of the array.
            this.mergeSort(reference, temporary, low, mid, index);

            // Recursively subdivide the upper half of the array.
            this.mergeSort(reference, temporary, mid + 1, high, index);

            // Merge the results of this level of subdivision.
            for (i = mid + 1; i > low; i--) {
                temporary[i - 1] = reference[i - 1];
            }

            for (j = mid; j < high; j++) {
                temporary[mid + (high - j)] = reference[j + 1]; // Avoid address overflow.
            }

            for (k = low; k <= high; k++) {
                reference[k]
                    = (this.superKeyCompare(temporary[i], temporary[j], index) < 0) ? temporary[i++] : temporary[j--];
            }
        }
    };

    /**
     * <p>
     * The {@code removeDuplicates} method} checks the validity of the merge
     * sort and removes from the reference array all but one of a set of
     * references that reference duplicate tuples.
     * </p>
     *
     * @param reference - an array of references to the (x,y,z,w...)
     * coordinates
     * @param p - the index of the most significant coordinate in the super
     * key
     * @returns number address of the last element of the references array
     * following duplicate removal
     *
     */
    KdTree.prototype.removeDuplicates = function (reference, p) {
        var end = 0;

        for (var i = 1; i < reference.length; i++) {
            var compare = this.superKeyCompare(reference[i], reference[i - 1], p);
            if (compare < 0) {
                $log.error("merge sort failure: superKeyCompare(ref[" + 0 + "]");
            }
            else if (compare > 0) {
                reference[++end] = reference[i];
            }
        }
        return end;
    };

    /**
     * <p>
     * Build a k-d tree by recursively partitioning the reference arrays and
     * adding nodes to the tree. These arrays are permuted cyclically for
     * successive levels of the tree in order that sorting use x, y, z, etc.
     * as the most significant portion of the sorting or partitioning key.
     * The contents of the reference arrays are scrambled by each recursive
     * partitioning.
     * </p>
     *
     * @param references - arrays of references to the (x,y,z,w...)
     * coordinates
     * @param temporary - a scratch array into which to copy references
     * @param start - the first element of the reference array a
     * @param end - the last element of the reference array a
     * @param depth - the depth in the k-d tree
     * @return KdNode {@link KdNode}
     */
    KdTree.prototype.buildKdTree = function (references, temporary, start, end, depth) {

        var i, j;
        var node = new KdNode();

        // The partition cycles as x, y, z, etc.
        var p = depth % references.length;

        if (end == start) {

            // Only one reference is passed to this method, so store it at this level of the tree.
            node = new KdNode(references[0][start]);

        } else if (end == start + 1) {

            // Two references are passed to this method in sorted order, so store the start
            // element at this level of the tree and store the end element as the > child.
            node = new KdNode(references[0][start]);
            node.right = new KdNode(references[0][end]);

        } else if (end == start + 2) {

            // Three references are passed to this method in sorted order, so
            // store the median element at this level of the tree, store the start
            // element as the < child and store the end element as the > child.
            node = new KdNode(references[0][start + 1]);
            node.left = new KdNode(references[0][start]);
            node.right = new KdNode(references[0][end]);

        } else if (end > start + 2) {

            // Four or more references are passed to this method.  Partitioning of the other reference
            // arrays will occur about the median element of references[0].  Avoid overflow when
            // calculating the median.  Store the median element of references[0] in a new k-d node.
            var median = start + ((end - start) >> 1);
            if (median <= start || median >= end) {
                $log.error("error in median calculation at depth = " + depth
                    + " : start = " + start + "  median = " + median + "  end = " + end);
            }
            node = new KdNode(references[0][median]);

            // Copy a[0] to the temporary array before partitioning.
            for (i = start; i <= end; i++) {
                temporary[i] = references[0][i];
            }

            // Sweep through each of the other reference arrays in its a priori sorted order
            // and partition it into "less than" and "greater than" halves by comparing
            // super keys.  Store the result from references[i] in references[i-1], thus
            // permuting the reference arrays.  Skip the element of references[i] that
            // references a point that equals the point that is stored in the new k-d node.
            var lower = -1;
            var upper = -1;
            var lowerSave = -1;
            var upperSave = -1;
            for (i = 1; i < references.length; i++) {
                lower = start - 1;
                upper = median;
                for (j = start; j <= end; j++) {

                    // Process one reference array.  Compare once only.
                    var compare = this.superKeyCompare(references[i][j], node.point.coordinates, p);
                    if (compare < 0) {
                        references[i - 1][++lower] = references[i][j];
                    } else if (compare > 0) {
                        references[i - 1][++upper] = references[i][j];
                    }
                }

                // Check the new indices for the reference array.
                if (lower != median - 1) {
                    $log.error("incorrect range for lower at depth - " + depth
                        + " : first = " + start + "  lower = " + lower + "  median = " + median);
                }

                if (upper != end) {
                    $log.error("incorrect range for upper at depth = " + depth
                        + " : median = " + median + "  upper = " + upper + "  end = " + end);
                }
                if (i > 1 && lower != lowerSave) {
                    $log.error("lower = " + lower + "  !=  lowerSave = " + lowerSave);
                }

                if (i > 1 && upper != upperSave) {
                    $log.error("upper = " + upper + "  !=  upperSave = " + upperSave);
                }

                lowerSave = lower;
                upperSave = upper;
            }

            // Copy the temporary array to the last reference array to finish permutation.  The copies to
            // and from the temporary array produce the O((k+1)n log n)  term of the computational
            // complexity.  This term may be reduced to a O((k-1)n log n) term for (x,y,z) coordinates
            // by eliminating these copies and explicitly passing x, y, z and t (temporary) arrays to this
            // buildKdTree method, then copying t<-x, x<-y and y<-z, then explicitly passing x, y, t and z
            // to the next level of recursion.  However, this approach would sacrifice the generality
            // of sorting points of any number of dimensions because explicit calling parameters
            // would need to be passed to this method for each specific number of dimensions.
            for (i = start; i <= end; i++) {
                references[references.length - 1][i] = temporary[i];
            }

            // Recursively build the < branch of the tree.
            node.left = this.buildKdTree(references, temporary, start, lower, depth + 1);

            // Recursively build the > branch of the tree.
            node.right = this.buildKdTree(references, temporary, median + 1, upper, depth + 1);

        } else if (end < start) {

            // This is an illegal condition that should never occur, so test for it last.
            $log.error("end < start");

        } else {

            // This final else block is added to keep the Java compiler from complaining.
            $log.error("unknown configuration of  start and end");
        }

        return node;

    };

    /**
     * <p>
     * The {@code createKdTree} method builds a k-d tree from an [][] of
     * points, where the coordinates of each point are stored as an [].
     * </p>
     *
     * @param coordinates - the int[][] of points
     */
    KdTree.prototype.createKdTree = function (coordinates) {
        var i, j;

        // If no coordinates specified, return
        if(coordinates.length == 0) return;

        // Declare and initialize the reference arrays.  The number of dimensions may be
        // obtained from either coordinates[0].length or references.length.  The number
        // of points may be obtained from either coordinates.length or references[0].length.
        var references = this.createMultiArray(coordinates[0].length, coordinates.length, 0);

        for (i = 0; i < references.length; i++) {
            this.initializeReferences(coordinates, references[i]);
        }

        // Merge sort the index arrays.
        var temporary = [];
        for (i = 0; i < references.length; i++) {
            this.mergeSort(references[i], temporary, 0, coordinates.length - 1, i);
        }

        // Remove references to duplicate coordinates via one pass through each reference array.
        var end = [];
        for (i = 0; i < references.length; i++) {
            end[i] = this.removeDuplicates(references[i], i);
        }

        // Check that the same number of references was removed from each reference array.
        for (i = 0; i < end.length - 1; i++) {
            for (j = i + 1; j < end.length; j++) {
                if (end[i] != end[j]) {
                    $log.error("reference removal error");
                }
            }
        }

        // Build the k-d tree. and assign the root of the tree
        this.size = end[0] + 1;
        this.root = this.buildKdTree(references, temporary, 0, end[0], 0);
    };

    /**
     * <p>
     * The {@code searchKdTree} method searches the k-d tree and finds the
     * KdNodes that lie within a cutoff distance from a query node in all k
     * dimensions.
     * </p>
     *
     * @param node - the query node
     * @param query - the query rectangle
     * @param depth - the depth in the k-d tree
     * @return Array KdNode
     * that contains the k-d nodes that lie within the cutoff distance of
     * the query node
     */
    KdTree.prototype.searchKdTree = function (node, query, depth) {
        // The partition cycles as x, y, z, etc.
        var axis = depth % node.point.coordinates.length;

        // If the distance from the query node to the k-d node is within the query rectangle
        // in all k dimensions, add the k-d node to a list.
        var result = [];
        var inside = true;

        if (!query.containsPoint(node.point)) {
            inside = false;
        }

        if (inside) {
            result.push(node);
        }

        // Search the < branch of the k-d tree if the partition coordinate of the query point minus
        // the cutoff distance is <= the partition coordinate of the k-d node.  The < branch must be
        // searched when the cutoff distance equals the partition coordinate because the super key
        // may assign a point to either branch of the tree if the sorting or partition coordinate,
        // which forms the most significant portion of the super key, shows equality.
        if (node.left != null) {
            if (query.min.coordinates[axis] <= node.point.coordinates[axis]) {
                var leftKdNodes = this.searchKdTree(node.left, query, depth + 1);
                if (leftKdNodes.length) result = result.concat(leftKdNodes)
            }
        }

        // Search the > branch of the k-d tree if the partition coordinate of the query point plus
        // the cutoff distance is >= the partition coordinate of the k-d node.  The < branch must be
        // searched when the cutoff distance equals the partition coordinate because the super key
        // may assign a point to either branch of the tree if the sorting or partition coordinate,
        // which forms the most significant portion of the super key, shows equality.
        if (node.right != null) {
            if (query.max.coordinates[axis] >= node.point.coordinates[axis]) {
                var rightKdNodes = this.searchKdTree(node.right, query, depth + 1);
                if (rightKdNodes.length) result = result.concat(rightKdNodes);
            }
        }

        return result;
    };


    /**
     * Performs merge sort on points and uses the euclidean distance as the sort criteria
     * @param a - list of points
     * @param b - temporary array
     * @param low - the
     * @param high
     * @param target
     */
    KdTree.prototype.mergeSortNodesByDistance = function (a, b, low, high, target) {
        var i, j, k;

        if (high > low) {

            var mid = low + ((high - low) >> 1);

            this.mergeSortNodesByDistance(a, b, low, mid, target);
            this.mergeSortNodesByDistance(a, b, mid + 1, high, target);

            for (i = mid + 1; i > low; i--) {
                b[i - 1] = a[i - 1];
            }

            for (j = mid; j < high; j++) {
                b[high + mid - j] = a[j + 1];
            }

            for (k = low; k <= high; k++) {
                var d1 = target.euclideanDistanceTo(b[i].point);
                var d2 = target.euclideanDistanceTo(b[j].point);

                a[k] = d1 < d2 ? b[i++] : b[j--];
            }
        }
    };

    KdTree.prototype.sortNodesByDistance = function (nodes, target) {
        var aux1 = [], aux2 = [];

        for (var i = 0; i < nodes.length; i++) {
            aux1[i] = nodes[i];
        }

        this.mergeSortNodesByDistance(aux1, aux2, 0, nodes.length - 1, target);

        return aux1;
    };


    KdTree.prototype.calculateQueryRectangle = function (target, min, max) {
        var rect = new HyperRectangle(target.length);

        for (var i = 0; i < target.coordinates.length; i++) {
            rect.min.coordinates[i] = min.coordinates[i] + target.coordinates[i];;
            rect.max.coordinates[i] = max.coordinates[i] + target.coordinates[i];;
        }

        return rect;
    };

    /**
     * Performs a search for the nearest point to the target point, that lies withing the
     * range of a query hyper-rectangle
     * @param target - the target point
     * @param min - the min coordinate of the query hyper-rectangle
     * @param max - the max coordinate of the query hyper-rectangle
     * @returns {null} - the point that is the nearest to the target point
     */
    KdTree.prototype.findNearestPoint = function (target, min, max) {

        if(this.root == null) return null;

        var hTarget = new HyperPoint(target);
        var hMin = new HyperPoint(min);
        var hMax = new HyperPoint(max);
        var query = this.calculateQueryRectangle(hTarget, hMin, hMax);
        var nodes = this.searchKdTree(this.root, query, 0);


        if (nodes.length == 0) {
            return null;
        }

        var sortedNodes = this.sortNodesByDistance(nodes, hTarget);
        return sortedNodes[0]['point']['coordinates'];

    };


    angular.module("app").factory('KdTree2', function () {
        return KdTree;
    });

})(
    window,
    window.angular,
    window.angular.injector(['ng', 'app']).get("$log"),
    window.angular.injector(['ng', 'app']).get("HyperRectangle"),
    window.angular.injector(['ng', 'app']).get("HyperPoint")
);
