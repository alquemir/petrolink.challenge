(function (window, angular, Component, Vector2, Color) {

    var Ellipse = function (config) {
        this.config = angular.extend({}, config);

        this.id = this.config.id || "";
        this.xRadius = this.config.xRadius || 0;
        this.yRadius = this.config.yRadius || 0;
        this.center = this.config.center || new Vector2(0, 0);
        this.isFill = this.config.isFill || false;
        this.color = this.config.color || Color.RED;

        Component.call(this, this.id, this.config.renderer);

    };

    Ellipse.prototype = Object.create(Component.prototype);
    Ellipse.prototype.constructor = Ellipse;

    Ellipse.prototype.containsPoint = function (point) {
        if (this.xRadius <= 0 || this.yRadius <= 0) {
            return false;
        }

        var normalised = new Vector2(point.x - this.center.x, point.y - this.center.y);
        return ((normalised.x * normalised.x) / (this.xRadius * this.xRadius)) + ((normalised.y * normalised.y) / (this.yRadius * this.yRadius)) <= 1.0;

    };

    Ellipse.prototype.setCenter = function (x, y) {
        this.center.x = x;
        this.center.y = y;
    };

    Ellipse.prototype.render = function () {
        //var pPosition = this.renderer.camera.projectWorldToViewportSpacePoint(this.position);
        //this.renderer.renderingContext.plotCircle(pPosition.x, pPosition.y, this.radius, this.color, this.isFill, this.lineWidth);
    };

    angular.module("app").factory("Ellipse", function () {
        return Ellipse;
    });

})(
    window,
    window.angular,
    window.angular.injector(['ng', 'app']).get('Component'),
    window.angular.injector(['ng', 'app']).get('Vector2'),
    window.angular.injector(['ng', 'app']).get('Color')
);
