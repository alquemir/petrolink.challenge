(function (window, angular) {
    var Color = function (r, g, b) {
        this.r = r || 0;
        this.g = g || 0;
        this.b = b || 0;
    };

    Color.prototype.toHex = function () {
        var hexValue = ((1 << 24) | (this.r << 16) | (this.g << 8) | this.b).toString(16).slice(1);
        return "#" + hexValue.toUpperCase();
    };

    Color.RED = new Color(255, 0, 0);
    Color.GREEN = new Color(0, 255, 0);
    Color.BLUE = new Color(0, 255, 0);
    Color.WHITE = new Color(255, 255, 255);
    Color.BLACK = new Color(0, 0, 0);
    Color.LIGHT_GRAY = new Color(211, 211, 211);

    angular.module("app").factory("Color", function () {
        return Color;
    });

})(window, window.angular);
