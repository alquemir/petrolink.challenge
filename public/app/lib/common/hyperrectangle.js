(function (window, angular, HyperPoint) {

    var HyperRectangle = function (dimensions) {
        this.dimensions = dimensions || 0;
        this.min = new HyperPoint();
        this.max = new HyperPoint();

        //set min/max to infinity
        for (var i = 0; i < this.dimensions; ++i) {
            this.min.coordinates[i] = Number.NEGATIVE_INFINITY;
            this.max.coordinates[i] = Number.POSITIVE_INFINITY;
        }
    };

    // from Moore's eqn. 6.6
    HyperRectangle.prototype.closest = function (t) {
        var p = new HyperPoint(t.coordinates.length, []);

        for (var i = 0; i < t.coordinates.length; ++i) {
            if (t.coordinates[i] <= this.min.coordinates[i]) {
                p.coordinates[i] = this.min.coordinates[i];
            }
            else if (t.coordinates[i] >= this.max.coordinates[i]) {
                p.coordinates[i] = this.max.coordinates[i];
            }
            else {
                p.coordinates[i] = t.coordinates[i];
            }
        }

        return p;
    };

    HyperRectangle.prototype.containsPoint = function (point) {
        var contains = true;

        for(var i = 0; i < point.coordinates.length; i++) {
            if(point.coordinates[i] < this.min.coordinates[i] || point.coordinates[i] > this.max.coordinates[i]) {
                contains = false;
                break;
            }
        }

        return contains;

    };

    HyperRectangle.prototype.clone = function () {
        return _.cloneDeep(this);
    };


    angular.module("app").factory("HyperRectangle", function () {
        return HyperRectangle;
    });

})(
    window,
    window.angular,
    window.angular.injector(['ng', 'app']).get("HyperPoint")
);
