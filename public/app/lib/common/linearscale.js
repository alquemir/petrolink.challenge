(function (window, angular, Scale) {

    var LinearScale = function (config) {
        this.config = angular.extend({}, config);
        Scale.call(this, this.config);
    };

    LinearScale.prototype = Object.create(Scale.prototype);
    LinearScale.prototype.constructor = LinearScale;


    LinearScale.prototype.init = function () {

        // Test for trivial condition of range = 0 and pick a suitable default

        if (this.max - this.min < 0) {
            this.max = this.max * (this.max === 0 ? 1.0 : Math.abs(this.max));
            this.min = this.min * (this.min === 0 ? 1.0 : Math.abs(this.min));
        }

        // Calculate the new step size
        this.majorTickSpacing = this.calculateTickSpacing(this.min, this.max, this.totalMajorTicks);
        this.minorTickSpacing = this.calculateTickSpacing(0, this.majorTickSpacing, this.totalMinorTicks);

        if (this.enableAutoMinorTick) {
            this.min = this.calculateDomainMinimumValue();
        }

        if (this.enableAutoMinorTick) {
            this.max = this.calculateDomainMaximumValue();
        }

    };

    LinearScale.prototype.printValue = function(value) {
        return value;
    };


    angular.module("app").factory("LinearScale", function () {
        return LinearScale;
    });
})(
    window,
    window.angular,
    window.angular.injector(['ng', 'app']).get('Scale')
);
