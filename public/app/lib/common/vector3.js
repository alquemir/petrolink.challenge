(function (window, angular) {
    var Vector3 = function (x, y, z) {
        this.x = x || 0;
        this.y = y || 0;
        this.z = z || 0;
    };

    Vector3.prototype.cross = function (vec) {

        return new Vector3(
            this.y * vec.z - this.z * vec.y,
            this.z * vec.x - this.x * vec.z,
            this.x * vec.y - this.y * vec.x
        );
    };

    Vector3.prototype.dot = function (vec) {
        return this.x * vec.x + this.y * vec.y + this.z * vec.z;
    };

    Vector3.prototype.mag = function () {
        return Math.sqrt(this.x * this.x + this.y * this.y * this.z * this.z)
    };

    Vector3.prototype.normalise = function () {
        var inverseMangnitude = 1 / this.mag();
        return new Vector3(
            this.x * inverseMangnitude,
            this.y * inverseMangnitude,
            this.z * inverseMangnitude
        );
    };

    angular.module("app").factory('Vector3', function () {
        return Vector3;
    });
})(window, window.angular);
