(function (window, angular, Vector3) {

    var Vector2 = function (x, y) {
        this.x = x || 0;
        this.y = y || 0;
    };

    Vector2.prototype.distanceToLineSegment = function (line) {
        var distance = this.squaredDistanceToLineSegment(line);
        return Math.sqrt(distance);
    };

    Vector2.prototype.squaredDistanceToLineSegment = function (line) {
        var v = new Vector2();  // v: P1 - P0 line segment vector
        var w = new Vector2();  // w: Q - P0

        v.x = line[1].x - line[0].x;
        v.y = line[1].y - line[0].y;

        w.x = this.x - line[0].x;
        w.y = this.y - line[0].y;

        var t = w.dot(v) / v.dot(v); // determine the closest point on line segment to point
        if(t <= 0) { // If the closest point is P0 the distance is w = ||Q - P0||^2, ||w||^2  w*w
            w.dot(w);
        } else if (t >= 1) { // Else closest point is P1, the distance is
                             // given by distsqd(Q, P1) = ((Q − P0) − v) · ((Q − P0) − v) = w · w − 2w· v + v · v
            return w.dot(w) - 2 * w.dot(v) + v.dot(v);
        } else { // The point lies on the segment, use general line-point distance formula, d = w · w − (w· v)^2 / v · v
            return w.dot(w) - (w.dot(v) * w.dot(v)) / v.dot(v);
        }

    };

    Vector2.prototype.cross = function (vec) {
        return this.x * vec.y - this.y * vec.x;
    };

    Vector2.prototype.dot = function (vec) {
        return this.x * vec.x + this.y * vec.y;
    };

    Vector2.prototype.mag = function () {
        return Math.sqrt(this.x * this.x + this.y * this.y)
    };

    Vector2.prototype.distanceSquaredTo = function (point) {
        var dx = point.x - this.x;
        var dy = point.y - this.y;

        return dx * dx + dy * dy;
    };

    /**
     * Calculates the euclidean distance from to the current point to another one
     */
    Vector2.prototype.distanceTo = function (point) {
        return Math.sqrt(this.distanceSquaredTo(point));
    };

    /**
     * Compares this point to the specified point.
     */
    Vector2.prototype.equals = function (point) {
        return this.x == point.x && this.y == point.y;
    };

    Vector2.prototype.normalise = function () {
        var inverseMangnitude = 1 / this.mag();
        return new Vector2(
            this.x * inverseMangnitude,
            this.y * inverseMangnitude
        );
    };

    Vector2.prototype.toHomogeneousCoordinates = function () {
        return new Vector3(
            this.x,
            this.y,
            1
        );
    };

    angular.module("app").factory("Vector2", function () {
        return Vector2;
    });
})(
    window,
    window.angular,
    angular.injector(['ng', 'app']).get('Vector3')
);
