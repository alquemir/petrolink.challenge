(function (window, angular) {

    var HyperPoint = function (coordinates, key) {
        this.coordinates = coordinates || [];
        this.key = key || undefined;
    };

    HyperPoint.prototype.copy = function (p) {
        for (var i = 0; i < this.coordinates.length; i++) {
            this.coordinates[i] = p.coordinates[i];
        }
    };

    HyperPoint.prototype.equals = function (p) {
        var isEqual = true;

        for (var i = 0; i < this.coordinates.length; i++) {
            if (this.coordinates[i] != p.coordinates[i]) {
                isEqual = false;
                break;
            }
        }

        return isEqual;
    };

    HyperPoint.prototype.distanceSquaredTo = function (p) {
        var distance = 0;

        for (var i = 0; i < p.coordinates.length; i++) {
            var d = p.coordinates[i] - this.coordinates[i];
            distance += d * d;
        }

        return distance;
    };

    HyperPoint.prototype.euclideanDistanceTo = function (p) {
        return Math.sqrt(this.distanceSquaredTo(p));
    };

    HyperPoint.prototype.print = function () {
        var ch = 'x';
        var coords = {};

        for(var i = 0; i < this.coordinates.length; i++) {
            coords[ch] = this.coordinates[i];
            ch = String.fromCharCode(ch.charCodeAt(0) + 1);
        }

        return coords;
    };

    angular.module("app").factory("HyperPoint", function () {
        return HyperPoint;
    });

})(
    window,
    window.angular
);
