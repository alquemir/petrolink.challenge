(function (window, angular, moment, $log, Scale) {
    var DateTimeScale = function (config) {
        this.config = angular.extend({}, config);
        this.majorUnit = DateTimeScale.DATEUNIT.YEAR;
        this.minorUnit = DateTimeScale.DATEUNIT.MONTH;
        Scale.call(this, this.config);
    };

    DateTimeScale.prototype = Object.create(Scale.prototype);
    DateTimeScale.prototype.constructor = DateTimeScale;

    DateTimeScale.DATE_FORMAT = {
        SECOND: "YYYY-MM-DD HH:mm:ss",
        MINUTE: "YYYY-MM-DD HH:mm:ss",
        HOUR: "YYYY-MM-DD HH:mm:ss",
        DAY: "YYYY-MM-DD HH:mm:ss",
        WEEK: "YYYY-MM-DD HH:mm:ss",
        MONTH: "YYYY-MM-DD HH:mm:ss",
        YEAR: "YYYY-MM-DD HH:mm:ss"
    };

    DateTimeScale.DATEUNIT = {
        SECOND: "SECOND",
        MINUTE: "MINUTE",
        HOUR: "HOUR",
        DAY: "DAY",
        WEEK: "WEEK",
        MONTH: "MONTH",
        YEAR: "YEAR"
    };

    //Total seconds in each time resolution
    DateTimeScale.TOTAL_MILLISECONDS = {
        SECOND: 1000,
        MINUTE: 60 * 1000,
        HOUR: 60 * 60 * 1000,
        DAY: 24 * 60 * 60 * 1000,
        WEEK: 7 * 24 * 60 * 60 * 1000,
        MONTH: 30 * 24 * 60 * 60 * 1000,
        YEAR: 365.2425 * 24 * 60 * 60 * 1000
    };


    DateTimeScale.TIMELEVEL = {
        SECOND: 1 * DateTimeScale.TOTAL_MILLISECONDS.SECOND,  // 5 seconds
        MINUTES: 2 * DateTimeScale.TOTAL_MILLISECONDS.MINUTE, // 10 minutes
        HOURS: 2 * DateTimeScale.TOTAL_MILLISECONDS.HOUR, // 2 hours
        DAYS: 2 * DateTimeScale.TOTAL_MILLISECONDS.DAY, // 2 days
        WEEKS: 2 * DateTimeScale.TOTAL_MILLISECONDS.WEEK, // 2 weeks
        MONTHS: 2 * DateTimeScale.TOTAL_MILLISECONDS.MONTH, // 10 months
        YEARS: 2 * DateTimeScale.TOTAL_MILLISECONDS.YEAR  // 2 years resolution
    };

    DateTimeScale.prototype.init = function () {

        //if (!this.validateDomainValues()) return;

        this.min = moment(this.min).valueOf();
        this.max = moment(this.max).valueOf();

        this.calculateDateTimeTickSpacings();

        if (this.enableAutoMinorTick) {
            this.min = this.calculateDomainMinimumValue();
        }

        if (this.enableAutoMajorTick) {
            this.max = this.calculateDomainMaximumValue();
        }

    };

    DateTimeScale.prototype.validateDomainValues = function () {
        if (!moment(this.min).isValid()) {
            $log.error("Invalid \"min\" value");
            return false;
        }

        if (!moment(this.min).isValid()) {
            $log.error("Invalid \"min\" value");
            return false;
        }

        if (moment(this.min) > moment(this.max)) {
            $log.error("\"min\" date value is higher than the \"max\"");
            return false;
        }

        return true;
    };

    DateTimeScale.prototype.calculateDateTimeTickSpacing = function (min, max, dateUnit) {
        var rMin = min / DateTimeScale.TOTAL_MILLISECONDS[dateUnit];
        var rMax = max / DateTimeScale.TOTAL_MILLISECONDS[dateUnit];
        return this.calculateTickSpacing(rMin, rMax, this.totalMajorTicks) * DateTimeScale.TOTAL_MILLISECONDS[dateUnit];
    };

    DateTimeScale.prototype.calculateDateTimeTickSpacings = function () {
        var range = this.max - this.min;


        if (range <= DateTimeScale.TIMELEVEL.MINUTES) {  // use seconds if you have less than 2 minutes worth (1-120)
            this.majorUnit = DateTimeScale.DATEUNIT.SECOND;
            this.majorTickSpacing = this.calculateDateTimeTickSpacing(this.min, this.max, this.majorUnit);
        }
        else if (range <= DateTimeScale.TIMELEVEL.HOURS) {  // use minutes if you have less than 2 hours worth
            this.majorUnit = DateTimeScale.DATEUNIT.MINUTE;
            this.majorTickSpacing = this.calculateDateTimeTickSpacing(this.min, this.max, this.majorUnit);
        }
        else if (range <= DateTimeScale.TIMELEVEL.DAYS) {  // use hours if you have less than 2 days worth
            this.majorUnit = DateTimeScale.DATEUNIT.HOUR;
            this.majorTickSpacing = this.calculateDateTimeTickSpacing(this.min, this.max, this.majorUnit);
        }
        else if (range <= DateTimeScale.TIMELEVEL.WEEKS) { // use days if you have less than 2 weeks worth
            this.majorUnit = DateTimeScale.DATEUNIT.DAY;
            this.majorTickSpacing = this.calculateDateTimeTickSpacing(this.min, this.max, this.majorUnit);
        }
        else if (range <= DateTimeScale.TIMELEVEL.MONTHS) { //  use weeks if you have less than 2 months worth
            this.majorUnit = DateTimeScale.DATEUNIT.WEEK;
            this.majorTickSpacing = this.calculateDateTimeTickSpacing(this.min, this.max, this.majorUnit);
        }
        else if (range <= DateTimeScale.TIMELEVEL.YEARS) { // use months if you have less than 2 years worth
            this.majorUnit = DateTimeScale.DATEUNIT.MONTH;
            this.majorTickSpacing = this.calculateDateTimeTickSpacing(this.min, this.max, this.majorUnit);
        }
        else { // use years
            this.majorUnit = DateTimeScale.DATEUNIT.YEARS;
            this.majorTickSpacing = this.calculateDateTimeTickSpacing(this.min, this.max, this.majorUnit);
        }
    };

    DateTimeScale.prototype.printValue = function (value) {
        var dateFormat = DateTimeScale.DATE_FORMAT[this.majorUnit];
        return moment(value).format(dateFormat);
    };

    angular.module("app").factory("DateTimeScale", function () {
        return DateTimeScale;
    });
})(
    window,
    window.angular,
    moment,
    window.angular.injector(['ng', 'app']).get('$log'),
    window.angular.injector(['ng', 'app']).get('Scale')
);
