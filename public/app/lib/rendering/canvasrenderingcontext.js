(function (window, angular) {

    var CanvasRenderingContext = function (canvasHtmlElement) {
        this.canvasHtmlElement = canvasHtmlElement || {};
        this.canvasContext = canvasHtmlElement.getContext("2d");
    };


    CanvasRenderingContext.prototype.plotPath = function (points, color, isFill, isStroke, lineWidth) {
        isFill = isFill || false;
        isStroke = isStroke || true;
        lineWidth = lineWidth || 0.5;

        this.canvasContext.save();
        this.canvasContext.beginPath();


        this.canvasContext.moveTo(points[0].x, points[0].y);

        for (var i = 1; i < points.length; i++) {
            this.canvasContext.lineTo(points[i].x, points[i].y);
        }

        this.canvasContext.closePath();


        if (isFill) {
            this.canvasContext.fillStyle = color.toHex();
            this.canvasContext.fill();
        }

        if (isStroke) {
            this.canvasContext.strokeStyle = color.toHex();
            this.canvasContext.lineWidth = lineWidth;
            this.canvasContext.stroke();
        }


        this.canvasContext.restore();
    };

    CanvasRenderingContext.prototype.plotCircle = function (x, y, radius, color, isFill, lineWidth) {
        isFill = isFill || false;
        lineWidth = lineWidth || 0.5;

        this.canvasContext.save();
        this.canvasContext.beginPath();

        this.canvasContext.arc(x, y, radius, 0, 2 * Math.PI);

        if (isFill) {
            this.canvasContext.fillStyle = color.toHex();
            this.canvasContext.fill();
        }
        else {
            this.canvasContext.strokeStyle = color.toHex();
            this.canvasContext.lineWidth = lineWidth;
            this.canvasContext.stroke();
        }


        this.canvasContext.stroke();
        this.canvasContext.restore();
    };


    CanvasRenderingContext.prototype.plotLine = function (p1, p2, color, lineWidth) {
        lineWidth = lineWidth || 0.5;

        this.canvasContext.save();
        this.canvasContext.beginPath();

        this.canvasContext.moveTo(p1.x, p1.y);
        this.canvasContext.lineTo(p2.x, p2.y);

        this.canvasContext.closePath();

        this.canvasContext.strokeStyle = color.toHex();
        this.canvasContext.lineWidth = lineWidth;
        this.canvasContext.stroke();

        this.canvasContext.restore();
    };

    CanvasRenderingContext.prototype.plotText = function (x, y, text, fontSize, isBold, color, isFill, textAlign, textBaseline, translationOffset, textRotationAngle) {
        isFill = isFill || true;
        color = color || color.BLACK;
        textAlign = textAlign || "start";
        textBaseline = textBaseline || "alphabetic";

        this.canvasContext.save();

        this.canvasContext.font = [isBold ? "bold" : "normal", " ", fontSize, "px arial"].join('');
        this.canvasContext.fillStyle = color.toHex();
        this.canvasContext.textAlign = textAlign;
        this.canvasContext.textBaseline = textBaseline;

        if(translationOffset) {
            this.canvasContext.translate(translationOffset.x, translationOffset.y);
        }

        if(textRotationAngle) {
            this.canvasContext.rotate(textRotationAngle * Math.PI / 180); //rotation angle is assumed to be in degrees, so we need to convert to radians
        }

        if(translationOffset) {
            this.canvasContext.translate(-translationOffset.x, -translationOffset.y);
        }

        if (isFill) {
            this.canvasContext.fillText(text, x, y);
        }
        else {
            this.canvasContext.strokeText(text, x, y);
        }

        this.canvasContext.restore();
    };

    CanvasRenderingContext.prototype.clear = function () {
        this.canvasContext.clearRect(0, 0, this.canvasHtmlElement.width, this.canvasHtmlElement.height);
    };


    angular.module("app").factory("CanvasRenderingContext", function () {
        return CanvasRenderingContext;
    });

})(window, window.angular);
