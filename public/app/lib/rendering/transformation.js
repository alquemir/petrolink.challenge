(function (window, angular, Matrix3) {
    var Transformation = function (component) {
        this.component = component || {};
        this.matrix = Matrix3.getIdentity(); //The transformation matrix
    };

    Transformation.prototype.translate = function (xT, yT) {
        var tMatrix = Matrix3.getTranslationMatrix(xT, yT);
        this.matrix = this.matrix.multiplyMatrix3(tMatrix);
    };

    Transformation.prototype.translateX = function (xT) {
        this.translate(xT, 0);
    };

    Transformation.prototype.translateY = function (yT) {
        this.translate(0, yT);
    };

    Transformation.prototype.scale = function (xS, yS) {
        var tMatrix = Matrix3.getScaleMatrix(xS, yS);
        this.matrix = this.matrix.multiplyMatrix3(tMatrix)
    };

    Transformation.prototype.scaleX = function (xS) {
        this.scale(xS, 1);
    };

    Transformation.prototype.scaleY = function (yS) {
        this.scale(1, yS);
    };

    Transformation.prototype.reset = function () {
        var tMatrix = this.matrix.getInverse();
        this.matrix = this.matrix.multiplyMatrix3(tMatrix);
    };

    Transformation.prototype.apply = function (points) {
        var self = this;
        return points.map(function(point) {
            return self.matrix.multiplyVector2(point);
        });
    };

    angular.module("app").factory("Transformation", function () {
        return Transformation;
    });

})(
    window,
    window.angular,
    window.angular.injector(["ng", "app"]).get("Matrix3")
);
