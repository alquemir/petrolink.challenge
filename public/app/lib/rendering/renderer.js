(function (window, _, angular, $rootScope, $interval, $log, Rectangle, Color, Camera, CanvasRenderingContext) {

    var Renderer = function (config) {

        this.config = angular.extend({}, config);
        this.components = [];
        this.htmlElementId = this.config.htmlElementId || "";
        this.htmlElement = undefined;
        this.renderingContext = undefined;
        this.renderingRequestId = undefined;
        this.clippingWindowWidth = this.config.clippingWindowWidth || 100;
        this.clippingWindowHeight = this.config.clippingWindowHeight || 100;
        this.camera = new Camera(this);
        this.isRendering = false;

    };


    Renderer.prototype.init = function () {
        var self = this;

        this.setupHtmlContainer();
        this.setupRenderingContext();
        this.setupCamera();

        window.addEventListener('resize', function() {
            self.setupHtmlContainer();
            self.setupCamera();
        }, false);
    };

    Renderer.prototype.setupCamera = function () {
        var bBox = this.htmlElement.getBoundingClientRect();

        this.camera.setupViewport(bBox.width, bBox.height);
        this.camera.setupClippingWindow(this.clippingWindowWidth, this.clippingWindowHeight);
    };


    Renderer.prototype.render = function () {
        var self = this;

        self.renderingContext.clear();
        self.renderComponents();

        self.renderingRequestId = window.requestAnimationFrame(function() {
            self.render();
        });
    };

    Renderer.prototype.startRendering = function () {
        this.render();
        this.isRendering = true;
    };

    Renderer.prototype.setupHtmlContainer = function () {
        this.htmlElement = window.document.getElementById(this.htmlElementId);

        this.htmlElement.style.width = "100%";
        this.htmlElement.style.height = "100%";

        this.htmlElement.width = this.htmlElement.offsetWidth;
        this.htmlElement.height = this.htmlElement.offsetHeight;
    };

    Renderer.prototype.stopRendering = function () {
        if (angular.isDefined(this.renderingRequestId)) {
            window.cancelAnimationFrame(this.renderingRequestId);
            this.renderingRequestId = undefined;
            this.isRendering = false;

            console.log("rendering stopped ...");
        }
    };

    Renderer.prototype.renderComponents = function () {
        this.components.forEach(function (component) {
            component.render();
        });
    };

    Renderer.prototype.setupRenderingContext = function (type) {
        this.renderingContext = new CanvasRenderingContext(this.htmlElement);
    };

    Renderer.prototype.getRenderingContext = function () {
        return this.renderingContext;
    };


    Renderer.prototype.addComponent = function (component) {
        if(this.findComponentById(component.id) != undefined) {
            $log("Component has already been registered ...");
            return;
        }

        component.setRenderer(this);
        this.components.push(component);
    };

    Renderer.prototype.findComponentById = function (id) {
        return _.find(this.components, function (component) {
            return component.id === id;
        });
    };


    angular.module("app").factory('Renderer', function () {
        return Renderer;
    });
})(
    window,
    window._,
    window.angular,
    window.angular.injector(['ng', 'app']).get('$rootScope'),
    window.angular.injector(['ng', 'app']).get('$interval'),
    window.angular.injector(['ng', 'app']).get('$log'),
    window.angular.injector(['ng', 'app']).get('Rectangle'),
    window.angular.injector(['ng', 'app']).get('Color'),
    window.angular.injector(['ng', 'app']).get('Camera'),
    window.angular.injector(['ng', 'app']).get('CanvasRenderingContext')
);
