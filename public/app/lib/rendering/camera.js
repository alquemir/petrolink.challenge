(function (window, angular, $rootScope, Vector2, Matrix3, Rectangle) {
    var Camera = function (renderer) {
        this.renderer = renderer;
        this.zoomFactor = 0.5;
        this.panFactor = 0.5;
        this.isPanning = false;
    };

    Camera.prototype.buildNormalisedDeviceToViewportSpaceMatrix = function () {
        return Matrix3.getNdcToScreenSpaceMatrix(
            this.viewport.left,
            this.viewport.right,
            this.viewport.bottom,
            this.viewport.top
        );
    };

    Camera.prototype.buildWorldToNormalisedDeviceSpaceMatrix = function () {
        return Matrix3.getWorldToNdcSpaceMatrix(
            this.clippingWindow.left,
            this.clippingWindow.right,
            this.clippingWindow.bottom,
            this.clippingWindow.top
        );
    };

    Camera.prototype.projectWorldToViewportSpacePoints = function (points) {
        var self = this;
        return points.map(function (point) {
            return self.projectWorldToViewportSpacePoint(point);
        });
    };

    Camera.prototype.projectWorldToViewportSpacePoint = function (point) {
        var tPoint = this.projectWorldToNormalisedDeviceSpacePoint(point);
        var tMatrix = this.buildNormalisedDeviceToViewportSpaceMatrix();

        return tMatrix.multiplyVector2(tPoint);
    };

    Camera.prototype.unprojectPointFromViewportToWorldSpace = function (point) {
        var tPoint = this.unprojectPointFromViewportToNormalisedDeviceSpace(point);
        var tMatrix = this.buildWorldToNormalisedDeviceSpaceMatrix().getInverse();

        return tMatrix.multiplyVector2(tPoint);
    };

    Camera.prototype.projectWorldToNormalisedDeviceSpacePoint = function (point) {
        var tMatrix = this.buildWorldToNormalisedDeviceSpaceMatrix();
        return tMatrix.multiplyVector2(point);
    };

    Camera.prototype.unprojectPointFromViewportToNormalisedDeviceSpace = function (point) {
        var tMatrix = this.buildNormalisedDeviceToViewportSpaceMatrix().getInverse();
        return tMatrix.multiplyVector2(point);
    };

    Camera.prototype.setupViewport = function (width, height) {
        this.viewport = new Rectangle();

        this.viewport.top = height;
        this.viewport.bottom = 0;
        this.viewport.left = 0;
        this.viewport.right = width;
    };

    Camera.prototype.setupClippingWindow = function (width, height) {

        this.clippingWindow = new Rectangle();
        this.clippingWindow.renderer = this.renderer;

        var ratio = this.viewport.getWidth() / this.viewport.getHeight();

        if(this.viewport.getWidth() >= this.viewport.getHeight()) {
            this.clippingWindow.left = (-width / 2) * ratio;
            this.clippingWindow.right = (width / 2) * ratio;
            this.clippingWindow.top = (height / 2);
            this.clippingWindow.bottom = (-height / 2);
        } else {
            this.clippingWindow.left = (-width / 2);
            this.clippingWindow.right = (width / 2);
            this.clippingWindow.top = (height / 2) / ratio;
            this.clippingWindow.bottom = (-height / 2) / ratio;
        }
    };

    Camera.prototype.getWorldMousePosition = function (x, y) {
        var mousePosition = this.getViewportMousePosition(x, y);
        return this.unprojectPointFromViewportToWorldSpace(mousePosition);
    };

    Camera.prototype.getViewportMousePosition = function (x, y) {
        var bBox = this.renderer.htmlElement.getBoundingClientRect();
        return new Vector2(
            x - bBox.left * (this.renderer.htmlElement.width / bBox.width),
            y - bBox.top * (this.renderer.htmlElement.height / bBox.height)
        );
    };


    angular.module("app").factory("Camera", function () {
        return Camera;
    });

})(
    window,
    window.angular,
    window.angular.injector(['ng', 'app']).get('$rootScope'),
    window.angular.injector(['ng', 'app']).get('Vector2'),
    window.angular.injector(['ng', 'app']).get('Matrix3'),
    window.angular.injector(['ng', 'app']).get('Rectangle')
);
