(function (window, angular, $timeout, Color, Vector2, Rectangle, KdTree) {
    var Tooltip = function (config) {
        this.config = angular.extend({}, config);
        this.graph = this.config.graph || {};
        this.htmlElement = undefined;
        this.renderer = this.config.graph.renderer || {};
        this.isVisible = false;

        this.cursor = new Rectangle({
            id: "cursor-1",
            renderer: this.graph.renderer
        });

        this.graph.renderer.addComponent(this.cursor);
    };

    Tooltip.prototype.getHtmlContent = function () {
        return "<div id=\"chart-tooltip\" class=\"tooltipster-base tooltipster-sidetip\">" +
            "<div class=\"tooltipster-box\">" +
            "<div class=\"tooltipster-content\">" +
            "<span></span>" +
            "</div>" +
            "</div>" +
            "<div class=\"tooltipster-arrow\">" +
            "<div class=\"tooltipster-arrow-uncropped\">" +
            "<div class=\"tooltipster-arrow-border\"></div>" +
            "<div class=\"tooltipster-arrow-background\"></div>" +
            "</div>" +
            "</div>" +
            "</div>";
    };

    Tooltip.prototype.setText = function (text) {
        this.spanHtmlElement.textContent = text;
    };

    Tooltip.prototype.init = function () {
        this.createHtmlElement();
        this.setupMouseEvents();
        this.hide();
    };

    Tooltip.prototype.createHtmlElement = function () {
        this.graph.renderer.htmlElement.insertAdjacentHTML('beforebegin', this.getHtmlContent());
        this.htmlElement = window.document.getElementById("chart-tooltip");
        this.spanHtmlElement = this.htmlElement.querySelector("span");
    };


    Tooltip.prototype.getPointPositionOnGraphViewportSpace = function (x, y) {
        var result = this.graph.getPointRangeValue(new Vector2(x, y));
        return this.renderer.camera.projectWorldToViewportSpacePoint(result);
    };

    Tooltip.prototype.moveTo = function (x, y) {
        this.htmlElement.style.position = "absolute";
        this.htmlElement.style.left = [x, "px"].join("");
        this.htmlElement.style.top = [y, "px"].join("");
    };

    Tooltip.prototype.hide = function () {
        this.htmlElement.style.display = "none";
        this.isVisible = false;
    };

    Tooltip.prototype.show = function () {
        var self = this;

        self.htmlElement.style.display = "";
        self.isVisible = true;
    };

    Tooltip.prototype.calculateQueryRectangleRange = function () {
        var dx = this.graph.xAxis.scale.majorTickSpacing / 10;
        var dy = this.graph.yAxis.scale.majorTickSpacing / 10;

        return {
            min: [-dx, -dy],
            max: [dx, dy]
        };
    };

    Tooltip.prototype.getPointLabel = function (point) {
        return [
            "X: ", this.graph.xAxis.scale.printValue(point[0]),
            "Y: ", this.graph.yAxis.scale.printValue(point[1])
        ].join(" ");
    };

    Tooltip.prototype.isPositionInsideGraphBoundaries = function (position) {
        return position.x >= this.graph.xAxis.scale.min && position.x <= this.graph.xAxis.scale.max
            && position.y >= this.graph.yAxis.scale.min && position.y <= this.graph.yAxis.scale.max
    };

    Tooltip.prototype.searchForPointWithinRange = function (x, y) {
        var mousePosition = this.graph.getLocalMouseWorldPosition(x, y);

        if(!this.isPositionInsideGraphBoundaries(mousePosition)) {
            if(this.isVisible) this.hide();
            return;
        }

        var range = this.calculateQueryRectangleRange();
        var nearestPoint = this.graph.tree.findNearestPoint([mousePosition.x, mousePosition.y], range.min, range.max);

        if (nearestPoint != null) {
            var position = this.getPointPositionOnGraphViewportSpace(nearestPoint[0], nearestPoint[1]);
            this.setText(this.getPointLabel(nearestPoint));
            this.moveTo(position.x, position.y);
            this.show();
        } else {
            this.hide();
        }

    };


    Tooltip.prototype.setupMouseEvents = function () {
        var self = this;

        window.addEventListener('mousemove', function (e) {
            self.searchForPointWithinRange(e.clientX, e.clientY);
        });

        window.addEventListener('wheel', function (e) {
            self.searchForPointWithinRange(e.clientX, e.clientY);
        });
    };

    angular.module("app").factory('Tooltip', function () {
        return Tooltip;
    });

})(
    window,
    window.angular,
    window.angular.injector(['ng', 'app']).get('$timeout'),
    window.angular.injector(['ng', 'app']).get('Color'),
    window.angular.injector(['ng', 'app']).get('Vector2'),
    window.angular.injector(['ng', 'app']).get('Rectangle'),
    window.angular.injector(['ng', 'app']).get('KdTree')
);
