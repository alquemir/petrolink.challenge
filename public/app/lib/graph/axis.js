(function (window, angular, Component, Vector2, Color, LinearScale, DateTimeScale) {

    var Axis = function (config) {
        this.config = angular.extend({}, config);
        this.scale = this.config.scale || new LinearScale();
        this.color = this.config.color || Color.BLACK;
        this.majorTickSize = this.config.majorTickSize || 1.0;
        this.tickFontSize = this.config.tickFontSize || 12;
        this.majorTickLineWidth = this.config.majorTickLineWidth || 1;
        this.minorTickSize = this.config.minorTickSize || (this.majorTickSize * 0.5);
        this.minorTickLineWidth = this.config.minorTickLineWidth || 0.5;
        this.orientation = this.config.orientation || Axis.ORIENTATION.HORIZONTAL;
        this.labelValue = this.config.labelValue || (this.orientation == Axis.ORIENTATION.HORIZONTAL ? "X Axis" : "Y Axis");
        this.labelFontSize = this.config.labelFontSize || (this.tickFontSize);
        this.graph = this.config.graph || {};

        Component.call(this, this.config.id, this.config.renderer);
    };

    //Parent init
    Axis.prototype = Object.create(Component.prototype);
    Axis.prototype.constructor = Axis;

    //Enums
    Axis.ORIENTATION = {
        HORIZONTAL: 0,
        VERTICAL: 1
    };

    Axis.LOCATION = {
        LEFT: 0,
        RIGHT: 1,
        TOP: 2,
        BOTTOM: 3
    };

    Axis.prototype.isTickVisible = function (tickValue) {
        return !(tickValue <= this.scale.min || tickValue >= this.scale.max);
    };

    Axis.prototype.plotLabel = function () {
        if (this.orientation === Axis.ORIENTATION.HORIZONTAL) {
            this.plotHorizontalLabel();
        } else {
            this.plotVerticalLabel();
        }
    };

    Axis.prototype.plotHorizontalLabel = function () {
        var position = new Vector2();
        var textAlign = "center";
        var textBaseline = "top";

        position.x = this.graph.rect.left + this.graph.rect.getWidth() / 2;
        position.y = this.graph.rect.bottom - this.graph.margin.bottom / 2;

        var projectedPosition = this.renderer.camera.projectWorldToViewportSpacePoint(position);
        this.renderer.renderingContext.plotText(projectedPosition.x, projectedPosition.y, this.labelValue, 12, true, Color.BLACK, true, textAlign, textBaseline);
    };

    Axis.prototype.plotVerticalLabel = function () {
        var position = new Vector2();
        var textAlign = "center";
        var textBaseline = "middle";

        position.x = this.graph.rect.left - this.graph.margin.left / 2;
        position.y = this.graph.rect.bottom + this.graph.rect.getHeight() / 2;

        var projectedPosition = this.renderer.camera.projectWorldToViewportSpacePoint(position);
        var translationOffset = new Vector2(projectedPosition.x, projectedPosition.y);
        this.renderer.renderingContext.plotText(projectedPosition.x, projectedPosition.y, this.labelValue, 12, true, Color.BLACK, true, textAlign, textBaseline, translationOffset, -90);
    };

    Axis.prototype.plotMajorTicks = function () {

        var totalTicks = this.scale.calculateTotalMajorTicks();
        var firstMajorTickValue = this.scale.calculateFirstMajorTickValue();

        for (var i = 0; i < totalTicks; i++) {
            var majorTickValue = this.scale.calculateMajorTickValue(firstMajorTickValue, i);

            // If we're before the start of the scale, just go to the next tick
            if (!this.isTickVisible(majorTickValue)) {
                continue;
            }

            if (this.orientation === Axis.ORIENTATION.HORIZONTAL) {
                this.plotHorizontalMajorTick(majorTickValue, this.isTickVisible(majorTickValue));
            } else {
                this.plotVerticalMajorTick(majorTickValue, this.isTickVisible(majorTickValue));
            }
        }
    };

    Axis.prototype.plotHorizontalMajorTick = function (majorTickValue) {
        var p1 = new Vector2();
        var p2 = new Vector2();
        var p3 = new Vector2();
        var p4 = new Vector2();

        p1.x = this.scale.getRangeValue(majorTickValue);
        p1.y = this.graph.rect.bottom;

        p2.x = this.scale.getRangeValue(majorTickValue);
        p2.y = this.graph.rect.bottom + this.majorTickSize;
        p3.x = this.scale.getRangeValue(majorTickValue);
        p3.y = this.graph.rect.top;

        p4.x = this.scale.getRangeValue(majorTickValue);
        p4.y = this.graph.rect.top - this.majorTickSize;

        this.plotTick(p1, p2, this.majorTickLineWidth);
        this.plotTick(p3, p4, this.majorTickLineWidth);

        this.plotTickLabel(p2.x, p2.y - this.majorTickSize * 2, majorTickValue, Axis.ORIENTATION.HORIZONTAL);
    };

    Axis.prototype.plotVerticalMajorTick = function (majorTickValue) {

        var p1 = new Vector2();
        var p2 = new Vector2();
        var p3 = new Vector2();
        var p4 = new Vector2();

        p1.x = this.graph.rect.left;
        p1.y = this.scale.getRangeValue(majorTickValue);

        p2.x = this.graph.rect.left + this.majorTickSize;
        p2.y = this.scale.getRangeValue(majorTickValue);

        p3.x = this.graph.rect.right;
        p3.y = this.scale.getRangeValue(majorTickValue);

        p4.x = this.graph.rect.right - this.majorTickSize;
        p4.y = this.scale.getRangeValue(majorTickValue);

        this.plotTick(p1, p2, this.majorTickLineWidth);
        this.plotTick(p3, p4, this.majorTickLineWidth);


        this.plotTickLabel(p2.x - this.majorTickSize * 2, p2.y, majorTickValue, Axis.ORIENTATION.VERTICAL)
    };

    Axis.prototype.plotTick = function (p1, p2, tickLineWidth) {

        var pP1 = this.renderer.camera.projectWorldToViewportSpacePoint(p1);
        var pP2 = this.renderer.camera.projectWorldToViewportSpacePoint(p2);

        this.renderer.renderingContext.plotLine(pP1, pP2, this.color, tickLineWidth);
    };

    Axis.prototype.plotTickLabel = function (x, y, value, orientation) {
        var textAlign,
            textBaseline;

        if (orientation == Axis.ORIENTATION.HORIZONTAL) {
            textAlign = "center";
            textBaseline = "top";
        } else {
            textAlign = "right";
            textBaseline = "middle";
        }

        var projectPosition = this.renderer.camera.projectWorldToViewportSpacePoint(new Vector2(x, y));
        this.renderer.renderingContext.plotText(projectPosition.x, projectPosition.y, this.scale.printValue(value), this.tickFontSize, false, Color.BLACK, true, textAlign, textBaseline);
    };

    Axis.prototype.render = function () {
        this.plotMajorTicks();
        this.plotLabel();
    };

    angular.module("app").factory("Axis", function () {
        return Axis;
    });

})(
    window,
    window.angular,
    window.angular.injector(['ng', 'app']).get("Component"),
    window.angular.injector(['ng', 'app']).get("Vector2"),
    window.angular.injector(['ng', 'app']).get("Color"),
    window.angular.injector(['ng', 'app']).get("LinearScale"),
    window.angular.injector(['ng', 'app']).get("DateTimeScale"));
