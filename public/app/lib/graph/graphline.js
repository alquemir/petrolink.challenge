(function (window, angular, _, Vector2, Color, Component, Square) {

    var GraphLine = function (config) {
        this.config = angular.extend({}, config);

        this.id = this.config.id || "";
        this.points = this.config.points || [];
        this.graph = this.config.graph || {};
        this.color = this.config.color || new Color(46, 127, 241);
        this.lineWidth = this.config.lineWidth || 2;
        this.showKnots = this.config.showKnots || true;
        this.knotSize = this.config.knotSize || 2;
        this.tolerance = 0;

        var self = this;
        window.addEventListener("keydown", function (e) {
            if (e.keyCode == '38') {
                // up arrow
                self.tolerance = self.tolerance < 1 ? self.tolerance + 0.01 : 1;
            }
            else if (e.keyCode == '40') {
                self.tolerance = self.tolerance > 0.01 ? self.tolerance - 0.01 : 0;
            }

            console.log(self.tolerance);
        });

        Component.call(this, this.id, this.config.renderer);
    };

    GraphLine.prototype = Object.create(Component.prototype);
    GraphLine.prototype.constructor = GraphLine;

    /**
     * Method to simplify the line, using the Ramer–Douglas–Peucker algorithm
     * Helpful when we want to reduce the LOD of the line, for instance when zooming
     * https://en.wikipedia.org/wiki/Ramer%E2%80%93Douglas%E2%80%93Peucker_algorithm
     * @param points
     * @param tolerance
     */

    GraphLine.prototype.simplifyLine = function (points, tolerance) {
        var i,
            index,
            start,
            end, maxDst, dst,
            startArray = [],
            endArray = [],
            resultPoints = [];


        if (points.length < 3) {
            return points;
        }

        var locations = _.fill(new Array(points.length), 0);

        start = 0;
        end = points.length - 1;

        locations[start] = 1;
        locations[end] = 1;

        while (end > 0) {
            maxDst = 0;

            for (i = start + 1; i < end; i++) {
                dst = points[i].squaredDistanceToLineSegment([points[start], points[end]]);

                if (dst > maxDst) {
                    index = i;
                    maxDst = dst;
                }
            }

            // If max distance is greater than tolerance  simplify
            if (maxDst > tolerance) {
                locations[index] = 1;

                startArray.push(start);
                endArray.push(index);

                startArray.push(index);
                endArray.push(end);
            }


            start = startArray.length == 0 ? 0 : startArray.pop();
            end = endArray.length == 0 ? 0 : endArray.pop();

        }

        for (i = 0; i < points.length; i++) {
            if (locations[i] != 0) {
                resultPoints.push(points[i]);
            }
        }

        return resultPoints;

    };

    GraphLine.prototype.calculateTolerance = function (points) {
        var inPoints = [], i;

        for (i = 0; i < points.length; i++) {

            if (points[i].x >= this.graph.xAxis.scale.min && points[i].x <= this.graph.xAxis.scale.max
                && points[i].y >= this.graph.yAxis.scale.min && points[i].y <= this.graph.yAxis.scale.max) {
                inPoints.push(points[i]);
            }
        }


        var dstTotal = 0;
        var start = 0;
        var end = inPoints.length - 1;

        for(i = start + 1; i < end; i++) {
            dstTotal += inPoints[i].squaredDistanceToLineSegment([inPoints[start], inPoints[end]]);
        }

        return dstTotal / (inPoints.length - 2);
    };

    GraphLine.prototype.clipPoints = function (p1, p2) {
        var rP1 = this.graph.getPointRangeValue(p1);
        var rP2 = this.graph.getPointRangeValue(p2);

        return this.graph.rect.clipLine(rP1, rP2);
    };


    GraphLine.prototype.plotLine = function (p1, p2) {
        var cPoints = this.clipPoints(p1, p2);
        if (!cPoints) return;

        var pPoints = this.renderer.camera.projectWorldToViewportSpacePoints(cPoints);
        return this.renderer.renderingContext.plotLine(pPoints[0], pPoints[1], this.color, this.lineWidth);
    };

    GraphLine.prototype.plotMark = function (position) {
        var square = new Square();
        var rPosition = this.graph.getPointRangeValue(position);

        square.color = this.color;
        square.isFill = true;
        square.size = this.knotSize;
        square.clipRect = this.graph.rect;
        square.renderer = this.renderer;
        square.transformation.translate(rPosition.x, rPosition.y);

        square.render();
    };

    GraphLine.prototype.render = function () {
        var points = this.graph.tree.getPoints({
            key: this.id,
            sortIndex: 0
        }).map(function (point) {
            return new Vector2(point[0], point[1]);
        });

        //points = this.simplifyLine(points, this.tolerance);
        //var t = this.calculateTolerance(points);
        //console.log(t);
        //points = this.simplifyLine(points, t);

        for (var i = 0; i < points.length - 1; i++) {
            this.plotLine(points[i], points[i + 1]);

            if (this.showKnots) {
                this.plotMark(points[i]);
            }

        }
    };

    angular.module("app").factory('GraphLine', function () {
        return GraphLine;
    });

})(
    window,
    window.angular,
    _,
    window.angular.injector(['ng', 'app']).get('Vector2'),
    window.angular.injector(['ng', 'app']).get('Color'),
    window.angular.injector(['ng', 'app']).get('Component'),
    window.angular.injector(['ng', 'app']).get('Square')
);
