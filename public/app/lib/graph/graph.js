(function (window, angular, _, Component, Axis, Rectangle, Vector2, LinearScale, DateTimeScale, Grid, GraphLine, ZoomPan, Tooltip, KdTree) {
    var Graph = function (config) {
        this.config = angular.extend({}, config);
        this.renderer = this.config.renderer;
        this.color = this.config.color || Color.BLACK;
        this.data = this.config.data || [];
        this.xAxisMode = this.config.xAxisMode || Graph.AxisMode.DATETIME;
        this.yAxisMode = this.config.yAxisMode || Graph.AxisMode.DECIMAL;
        this.xAxisLabel = this.config.xAxisLabel || "X Axis";
        this.yAxisLabel = this.config.yAxisLabel || "Y Axis";
        this.tree = new KdTree();
        this.graphLines = [];
        this.autoScale = true;
        this.minPoints = 2;

        this.rect = new Rectangle({
            renderer: this.config.renderer,
            color: this.color,
            lineWidth: 1.0
        });

        this.xAxis = new Axis({
            id: "x-axis-1",
            orientation: Axis.ORIENTATION.HORIZONTAL,
            labelValue: this.xAxisLabel,
            graph: this,
            color: this.config.color,
            renderer: this.config.renderer,
            majorTickLineWidth: 1
        });

        this.yAxis = new Axis({
            id: "y-axis-1",
            orientation: Axis.ORIENTATION.VERTICAL,
            labelValue: this.yAxisLabel,
            graph: this,
            color: this.config.color,
            renderer: this.config.renderer,
            majorTickLineWidth: 1
        });

        this.grid = new Grid({
            id: "grid-1",
            graph: this,
            renderer: this.config.renderer
        });

        this.margin = new Rectangle({
            top: 2,
            bottom: 20,
            right: 2,
            left: 20
        });

        this.zoomPan = new ZoomPan({
            graph: this
        });

        Component.call(this, this.config.id, this.config.renderer);
    };

    Graph.prototype = Object.create(Component.prototype);
    Graph.prototype.constructor = Graph;

    Graph.AxisMode = {
        DATETIME: "DATETIME",
        DECIMAL: "DECIMAL"
    };

    Graph.prototype.init = function () {
        var self = this;

        this.setupRectangle();
        this.parseData();

        this.setupXAxis();
        this.setupYAxis();

        this.setupZoomPan();
        this.setupTooltip();

        window.addEventListener('resize', function () {
            self.setupRectangle();
            self.setupXAxis();
            self.setupYAxis();
        }, false);
    };

    Graph.prototype.setupZoomPan = function () {
        this.zoomPan.enableHorizontalZoom = true;
        this.zoomPan.enableVerticalZoom = true;
        this.zoomPan.zoomFactor = 0.05;
        this.zoomPan.init();
    };

    Graph.prototype.setupXAxis = function () {
        this.xAxis.scale = this.xAxisMode == Graph.AxisMode.DATETIME ? new DateTimeScale() : new LinearScale();
        this.xAxis.scale.min = this.tree.size >= this.minPoints ? this.getXAxisMinimumValue() : 0;
        this.xAxis.scale.max = this.tree.size >= this.minPoints ? this.getXAxisMaximumValue() : 1;
        this.xAxis.scale.minRange = this.rect.left;
        this.xAxis.scale.maxRange = this.rect.right;
        this.xAxis.scale.init();
    };

    Graph.prototype.setupYAxis = function () {
        this.yAxis.scale = this.yAxisMode == Graph.AxisMode.DATETIME ? new DateTimeScale() : new LinearScale();
        this.yAxis.scale.min = this.tree.size >= this.minPoints ? this.getYAxisMinimumValue() : 0;
        this.yAxis.scale.max = this.tree.size >= this.minPoints ? this.getYAxisMaximumValue() : 1;
        this.yAxis.scale.minRange = this.rect.bottom;
        this.yAxis.scale.maxRange = this.rect.top;
        this.yAxis.scale.init();
    };

    Graph.prototype.getXAxisMinimumValue = function () {
        return this.tree.findMinimumPoint(0)[0];
    };

    Graph.prototype.getXAxisMaximumValue = function () {
        return this.tree.findMaximumPoint(0)[0];
    };

    Graph.prototype.getYAxisMinimumValue = function () {
        return this.tree.findMinimumPoint(1)[1];
    };

    Graph.prototype.getYAxisMaximumValue = function () {
        return this.tree.findMaximumPoint(1)[1];
    };

    Graph.prototype.getPointRangeValue = function (point) {
        var result = new Vector2();

        result.x = this.xAxis.scale.getRangeValue(point.x);
        result.y = this.yAxis.scale.getRangeValue(point.y);

        return result;
    };

    Graph.prototype.getPointDomainValue = function (point) {
        var result = new Vector2();

        result.x = this.xAxis.scale.getDomainValue(point.x);
        result.y = this.yAxis.scale.getDomainValue(point.y);

        return result;
    };

    Graph.prototype.createGraphLine = function (id) {
        var graphLine = new GraphLine({
            id: id,
            graph: this,
            points: [],
            renderer: this.renderer
        });

        this.graphLines.push(graphLine);
        return _.last(this.graphLines);
    };

    Graph.prototype.findGraphLineById = function (id) {
        return _.find(this.graphLines, function(graphLine) {
            return graphLine.id == id;
        });
    };

    Graph.prototype.parseDataValue = function (dataValue) {
        var values = _.values(dataValue);

        return [
            this.xAxisMode == Graph.AxisMode.DATETIME ? moment(values[0]).valueOf() : +values[0],
            this.yAxisMode == Graph.AxisMode.DATETIME ? moment(values[1]).valueOf() : +values[1]
        ];
    };

    Graph.prototype.parseData = function () {
        //convert to epoch timestamps if any of the values in the array is an date string
        var self = this;
        self.tree.insertPoints(self.data, "curve-1");


        var graphLine = this.findGraphLineById("curve-1");
        if(graphLine == null) {
            this.createGraphLine("curve-1");
        }
    };


    Graph.prototype.insertDataValue = function (dataValue, graphLineId) {


        var graphLine = this.findGraphLineById(graphLineId);
        if(!graphLine) {
            this.createGraphLine(graphLineId);
        }

        var value = this.parseDataValue(dataValue);
        this.tree.insertPoint(value, graphLineId);

        this.adjustScale();

    };

    Graph.prototype.adjustScale = function () {
        if(!this.autoScale) return;

        this.setupXAxis();
        this.setupYAxis();

        this.zoomPan.saveDefaultZoomPanState();
    };


    Graph.prototype.setupRectangle = function () {
        this.rect.isStroke = true;
        this.rect.isFill = false;
        this.rect.top = this.renderer.camera.clippingWindow.top - this.margin.top;
        this.rect.bottom = this.renderer.camera.clippingWindow.bottom + this.margin.bottom;
        this.rect.left = this.renderer.camera.clippingWindow.left + this.margin.left;
        this.rect.right = this.renderer.camera.clippingWindow.right - this.margin.right;
    };

    Graph.prototype.setupTooltip = function () {
        this.tooltip = new Tooltip({
            graph: this,
            renderer: this.config.renderer
        });

        this.tooltip.init();
    };

    Graph.prototype.getLocalMouseWorldPosition = function (x, y) {
        var result = new Vector2();
        var pos = this.renderer.camera.getWorldMousePosition(x, y);

        result.x = this.xAxis.scale.getDomainValue(pos.x);
        result.y = this.yAxis.scale.getDomainValue(pos.y);

        return result;
    };

    Graph.prototype.render = function () {
        this.grid.render();
        this.rect.render();
        this.xAxis.render();
        this.yAxis.render();

        //render graph lines
        this.graphLines.forEach(function (graphLine) {
            graphLine.render();
        });
    };

    angular.module("app").factory("Graph", function () {
        return Graph;
    });

})(
    window,
    window.angular,
    _,
    window.angular.injector(['ng', 'app']).get('Component'),
    window.angular.injector(['ng', 'app']).get('Axis'),
    window.angular.injector(['ng', 'app']).get('Rectangle'),
    window.angular.injector(['ng', 'app']).get('Vector2'),
    window.angular.injector(['ng', 'app']).get('LinearScale'),
    window.angular.injector(['ng', 'app']).get('DateTimeScale'),
    window.angular.injector(['ng', 'app']).get('Grid'),
    window.angular.injector(['ng', 'app']).get('GraphLine'),
    window.angular.injector(['ng', 'app']).get('ZoomPan'),
    window.angular.injector(['ng', 'app']).get('Tooltip'),
    window.angular.injector(['ng', 'app']).get('KdTree')

);
