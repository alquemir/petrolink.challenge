(function (window, angular) {
    var GraphLegend = function (config) {
        this.config = angular.extend({}, config);
        this.renderer = this.config.graph.renderer || {};
    };

    GraphLegend.prototype.init = function () {};

    angular.module("app").factory("Graph", function () {
        return GraphLegend;
    });

})(window, window.angular);