(function (window, angular, _, $timeout, Vector2) {
    var ZoomPan = function (config) {
        this.config = angular.extend({}, config);
        this.graph = this.config.graph || {};

        this.panFactor = this.config.panFactor || 0.1;
        this.enableHorizontalPan = this.config.enableHorizontalPan || true;
        this.enableVerticalPan = this.config.enableVerticalPan || true;
        this.isPanning = false;

        this.zoomFactor = this.config.zoomFactor || 0.1;
        this.isZooming = false;
        this.enableZoom = this.config.enableZoom || true;
        this.enableHorizontalZoom = this.config.enableHorizontalZoom || true;
        this.enableVerticalZoom = this.config.enableVerticalZoom || true;
        this.zoomPanStates = [];
    };

    var ZoomPanState = function (xScale, yScale, isDefault, zoomFraction) {
        this.isDefault = isDefault || false;
        this.xScale = xScale || null;
        this.yScale = yScale || null;
        this.zoomFraction = zoomFraction || 0;
    };

    ZoomPan.prototype.init = function () {
        this.saveDefaultZoomPanState();
        this.setupMouseEvents();
    };

    ZoomPan.prototype.getDefaultZoomPanState = function () {
        return _.find(this.zoomPanStates, function (zoomPanState) {
            return zoomPanState.isDefault;
        });
    };

    ZoomPan.prototype.restoreDefaultZoomPanState = function () {
        var defaultZoomPanState = this.getDefaultZoomPanState();
        this.applyState(defaultZoomPanState);
    };

    ZoomPan.prototype.getDefaultZoomPanStateIndex = function () {
        return _.findIndex(this.zoomPanStates, function (zoomPanState) {
            return zoomPanState.isDefault;
        });
    };

    ZoomPan.prototype.removeDefaultZoomPanState = function () {
        var index = this.getDefaultZoomPanStateIndex();
        this.zoomPanStates.splice(index);
    };

    ZoomPan.prototype.saveDefaultZoomPanState = function () {
        this.removeDefaultZoomPanState();

        var zoomPanState = new ZoomPanState();

        zoomPanState.xScale = this.graph.xAxis.scale;
        zoomPanState.yScale = this.graph.yAxis.scale;
        zoomPanState.isDefault = true;

        this.savePanZoomState(zoomPanState);
    };


    ZoomPan.prototype.savePanZoomState = function (zoomPanState) {
        this.zoomPanStates.push(zoomPanState);
    };

    ZoomPan.prototype.applyState = function (zoomPanState) {
        if (zoomPanState.xScale) {
            this.graph.xAxis.scale = zoomPanState.xScale;
            this.graph.xAxis.scale.init();
        }

        if (zoomPanState.yScale) {
            this.graph.yAxis.scale = zoomPanState.yScale;
            this.graph.yAxis.scale.init();
        }
    };

    ZoomPan.prototype.setupMouseEvents = function () {
        var self = this;


        window.addEventListener('mousedown', function (e) {
            self.isPanning = true;
            self.panStart = self.graph.getLocalMouseWorldPosition(e.clientX, e.clientY);
        });

        window.addEventListener('mouseup', function (e) {
            self.isPanning = false;
        });

        window.addEventListener('mousemove', function (e) {
            self.onMouseMovePanning(e);
        });

        var prefix = "", _addEventListener, _addWheelListener;


        // detect available wheel event
        var support = "onwheel" in document.createElement("div") ? "wheel" : // Modern browsers support "wheel"
            document.onmousewheel !== undefined ? "mousewheel" : // Webkit and IE support at least "mousewheel"
                "DOMMouseScroll"; // let's assume that remaining browsers are older Firefox


        window.addWheelListener = function (elem, callback, useCapture) {
            _addWheelListener(elem, support, callback, useCapture);

            // handle MozMousePixelScroll in older Firefox
            if (support == "DOMMouseScroll") {
                _addWheelListener(elem, "MozMousePixelScroll", callback, useCapture);
            }
        };

        // detect event model
        if (window.addEventListener) {
            _addEventListener = "addEventListener";
        } else {
            _addEventListener = "attachEvent";
            prefix = "on";
        }


        _addWheelListener = function (elem, eventName, callback, useCapture) {
            elem[_addEventListener](prefix + eventName, support == "wheel" ? callback : function (originalEvent) {
                !originalEvent && ( originalEvent = window.event );

                // create a normalized event object
                var event = {
                    // keep a ref to the original event object
                    originalEvent: originalEvent,
                    target: originalEvent.target || originalEvent.srcElement,
                    type: "wheel",
                    deltaMode: originalEvent.type == "MozMousePixelScroll" ? 0 : 1,
                    deltaX: 0,
                    deltaY: 0,
                    deltaZ: 0,
                    preventDefault: function () {
                        originalEvent.preventDefault ?
                            originalEvent.preventDefault() :
                            originalEvent.returnValue = false;
                    }
                };

                // calculate deltaY (and deltaX) according to the event
                if (support == "mousewheel") {
                    event.deltaY = -1 / 40 * originalEvent.wheelDelta;
                    // Webkit also support wheelDeltaX
                    originalEvent.wheelDeltaX && ( event.deltaX = -1 / 40 * originalEvent.wheelDeltaX );
                } else {
                    event.deltaY = originalEvent.detail;
                }

                // it's time to fire the callback
                return callback(event);

            }, useCapture || false);
        };

        window.addWheelListener(window, function (e) {
            self.onMouseWheelZooming(e);
        }, false);

    };

    ZoomPan.prototype.onMouseMovePanning = function (e) {
        if (this.isPanning) {
            var panEnd = this.graph.getLocalMouseWorldPosition(e.clientX, e.clientY);
            var zoomPanState = this.setupPanScale(this.panStart, panEnd);
            this.graph.autoScale = false;
            this.applyState(zoomPanState);
        }
    };

    ZoomPan.prototype.setupPanScale = function (startPosition, endPosition) {
        var zoomPanState = new ZoomPanState();

        if (this.enableHorizontalPan) {
            zoomPanState.xScale = this.calculatePanScale(this.graph.xAxis.scale, startPosition.x, endPosition.x);
        }

        if (this.enableVerticalPan) {
            zoomPanState.yScale = this.calculatePanScale(this.graph.yAxis.scale, startPosition.y, endPosition.y);
        }

        return zoomPanState;
    };

    ZoomPan.prototype.calculatePanScale = function (scale, startValue, endValue) {
        var tScale = _.cloneDeep(scale);
        var delta = (startValue - endValue) * this.panFactor;

        tScale.min += delta;
        tScale.max += delta;

        tScale.enableAutoMajorTick = false;
        tScale.enableAutoMinorTick = false;

        return tScale;
    };

    ZoomPan.prototype.getWheelDeltaFromEvent = function (e) {
        if (e.wheelDelta) {
            return e.wheelDelta;
        }

        if (e.originalEvent.detail) {
            return e.originalEvent.detail * -40;
        }

        if (e.originalEvent && e.originalEvent.wheelDelta) {
            return e.originalEvent.wheelDelta;
        }
    };

    ZoomPan.prototype.onMouseWheelZooming = function (e) {
        var centerPosition = this.graph.getLocalMouseWorldPosition(e.clientX, e.clientY);
        var zoomFraction = 1 + (this.getWheelDeltaFromEvent(e) < 0 ? 1 : -1) * this.zoomFactor;

        var zoomPanState = this.setupZoomScale(centerPosition.x, centerPosition.y, zoomFraction, false);
        this.graph.autoScale = false;
        this.applyState(zoomPanState);
    };

    ZoomPan.prototype.getCenterPosition = function () {
        var result = new Vector2();

        result.x = (this.graph.xAxis.scale.max - this.graph.xAxis.scale.min) / 2;
        result.y = (this.graph.yAxis.scale.max - this.graph.yAxis.scale.min) / 2;

        return result;
    };


    ZoomPan.prototype.zoomIn = function () {

        var centerPosition = this.getCenterPosition();
        var zoomFraction = 1 - this.zoomFactor;
        var zoomPanState = this.setupZoomScale(centerPosition.x, centerPosition.y, zoomFraction, false);

        this.applyState(zoomPanState);
    };

    ZoomPan.prototype.zoomOut = function () {

        var centerPosition = this.getCenterPosition();
        var zoomFraction = 1 + this.zoomFactor;
        var zoomPanState = this.setupZoomScale(centerPosition.x, centerPosition.y, zoomFraction, false);

        this.applyState(zoomPanState);
    };

    ZoomPan.prototype.zoomReset = function () {
        this.restoreDefaultZoomPanState();
    };


    ZoomPan.prototype.setupZoomScale = function (cX, cY, zoomFraction, zoomOnCenter) {
        var zoomPanState = new ZoomPanState();
        zoomPanState.zoomFraction = zoomFraction;

        if (this.enableHorizontalZoom) {
            zoomPanState.xScale = this.calculateZoomScale(this.graph.xAxis.scale, zoomFraction, cX, zoomOnCenter);
        }

        if (this.enableVerticalZoom) {
            zoomPanState.yScale = this.calculateZoomScale(this.graph.yAxis.scale, zoomFraction, cY, zoomOnCenter);
        }


        return zoomPanState;
    };

    ZoomPan.prototype.calculateZoomScale = function (scale, zoomFraction, centerValue, zoomOnCenter) {

        var rScale = _.cloneDeep(scale);
        var min = rScale.min;
        var max = rScale.max;

        var range = (max - min) * zoomFraction / 2.0;

        if (!zoomOnCenter) {
            centerValue = (max + min) / 2.0;
        }

        rScale.min = centerValue - range;
        rScale.max = centerValue + range;

        rScale.enableAutoMajorTick = false;
        rScale.enableAutoMinorTick = false;

        return rScale;
    };

    angular.module("app").factory("ZoomPan", function () {
        return ZoomPan;
    });

})(
    window,
    window.angular,
    _,
    window.angular.injector(['ng', 'app']).get('$timeout'),
    window.angular.injector(['ng', 'app']).get('Vector2')
);
