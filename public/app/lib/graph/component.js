(function (window, angular, Transformation) {

    /**
     * Component constructor
     */
    var Component = function (id, renderer) {
        this.id = id || "";
        this.renderer = renderer || {};
        this.transformation = new Transformation(this);
    };

    Component.prototype.setRenderer = function (renderer) {
        this.renderer = renderer;
    };

    Component.prototype.getRenderingContext = function () {
        return this.renderer.getRenderingContext();
    };

    Component.prototype.render = function () {};

    angular.module("app").factory("Component", function () {
        return Component;
    });

})(
    window,
    window.angular,
    window.angular.injector(["ng", "app"]).get("Transformation")
);
