(function (window, angular, Component, Color, Vector2) {

    /**
     * Draws a grid
     * @param {string} id    Id of the component
     * @param {Color}  color  Color of the grid
     * @param {Number} xStep [description]
     * @param {Number} yStep [description]
     * @param {Number} xMin  [description]
     * @param {Number} xMax  [description]
     * @param {Number} yMin  [description]
     * @param {Number} yMax  [description]
     */
    var Grid = function (config) {
        this.config = angular.extend({}, config);

        this.graph = this.config.graph || {};
        this.color = this.config.color || Color.LIGHT_GRAY;
        this.lineWidth = this.config.lineWidth || 0.5;

        Component.call(this, this.config.id, this.config.renderer);
    };

    Grid.prototype = Object.create(Component.prototype);
    Grid.prototype.constructor = Grid;

    Grid.prototype.isVerticalTickVisible = function (tickValue) {
        return !(tickValue <= this.graph.xAxis.scale.min || tickValue >= this.graph.xAxis.scale.max);
    };

    Grid.prototype.isHorizontalTickVisible = function (tickValue) {
        return !(tickValue <= this.graph.yAxis.scale.min || tickValue >= this.graph.yAxis.scale.max);
    };

    Grid.prototype.plotHorizontalGridLines = function () {
        var p1 = new Vector2();
        var p2 = new Vector2();
        var vP1 = new Vector2();
        var vP2 = new Vector2();
        var firstMajorTickValue = this.graph.yAxis.scale.calculateFirstMajorTickValue();

        for (var i = 0; i < this.graph.yAxis.scale.calculateTotalMajorTicks(); i++) {
            var tickValue = this.graph.yAxis.scale.calculateMajorTickValue(firstMajorTickValue, i);

            if (!this.isHorizontalTickVisible(tickValue)) {
                continue;
            }

            p1.x = this.graph.rect.left;
            p1.y = this.graph.yAxis.scale.getRangeValue(tickValue);

            p2.x = this.graph.rect.right;
            p2.y = this.graph.yAxis.scale.getRangeValue(tickValue);

            vP1 = this.graph.renderer.camera.projectWorldToViewportSpacePoint(p1);
            vP2 = this.graph.renderer.camera.projectWorldToViewportSpacePoint(p2);

            this.renderer.renderingContext.plotLine(vP1, vP2, this.color, this.lineWidth);
        }
    };

    Grid.prototype.plotVerticalGridLines = function () {
        var p1 = new Vector2();
        var p2 = new Vector2();
        var vP1 = new Vector2();
        var vP2 = new Vector2();
        var firstMajorTickValue = this.graph.xAxis.scale.calculateFirstMajorTickValue();

        for (var i = 0; i < this.graph.xAxis.scale.calculateTotalMajorTicks(); i++) {
            var tickValue = this.graph.xAxis.scale.calculateMajorTickValue(firstMajorTickValue, i);

            if (!this.isVerticalTickVisible(tickValue)) {
                continue;
            }

            p1.x = this.graph.xAxis.scale.getRangeValue(tickValue);
            p1.y = this.graph.rect.top;

            p2.x = this.graph.xAxis.scale.getRangeValue(tickValue);
            p2.y = this.graph.rect.bottom;

            vP1 = this.graph.renderer.camera.projectWorldToViewportSpacePoint(p1);
            vP2 = this.graph.renderer.camera.projectWorldToViewportSpacePoint(p2);

            this.renderer.renderingContext.plotLine(vP1, vP2, this.color, this.lineWidth);
        }
    };

    Grid.prototype.render = function () {

        //x-axis lines
        this.plotHorizontalGridLines();

        //y-axis
        this.plotVerticalGridLines();
    };


    angular.module("app").factory("Grid", function () {
        return Grid;
    });

})(
    window,
    window.angular,
    angular.injector(['ng', 'app']).get('Component'),
    angular.injector(['ng', 'app']).get('Color'),
    angular.injector(['ng', 'app']).get('Vector2')
);
