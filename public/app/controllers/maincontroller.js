(function (window, angular, app) {
    app.controller("MainController", ['$scope', '$timeout', 'MainService', function ($scope, $timeout, MainService) {
        $scope.isResumeButtonVisible = true;
        $scope.isPauseButtonVisible = false;
        $scope.isConnected = false;
        $scope.errorMessage = "";

        $scope.connect = function () {
            MainService.connectToServer().then(function () {
                $scope.isConnected = true;
                $timeout(function () {
                    MainService.setupRenderer();
                    MainService.setupGraph();

                    $scope.resume();
                }, 0);
            }, function () {
                $scope.showErrorMessage = true;
                $scope.errorMessage = "Error connecting to the server";
            });
        };

        $scope.resume = function () {
            $scope.isResumeButtonVisible = false;
            $scope.isPauseButtonVisible = true;

            MainService.resumeStream();
        };

        $scope.pause = function () {
            $scope.isResumeButtonVisible = true;
            $scope.isPauseButtonVisible = false;

            MainService.pauseStream();
        };

        $scope.zoomIn = function ($event) {
            $event.stopPropagation();
            MainService.zoomIn();
        };

        $scope.zoomOut = function ($event) {
            $event.stopPropagation();
            MainService.zoomOut();
        };

        $scope.zoomReset = function ($event) {
            $event.stopPropagation();
            MainService.zoomReset();
        };

        $scope.autoScale = function () {
            MainService.autoScale();
        };

    }]);

})(
    window,
    window.angular,
    window.angular.module("app")
);
