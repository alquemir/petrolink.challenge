(function (window, angular, io, app) {
    app.factory("MainService", ['$q', 'Renderer', 'Graph', 'Color', function ($q, Renderer, Graph, Color) {

        var data = [
            [4, 5],
            [5, 5],
            [1, 3],
            [2, 2],
            [2, 2],
            [4, 4]
        ];

        var socket = undefined;
        var graph = undefined;
        var renderer = undefined;

        var MainService = {};

        MainService.connectToServer = function () {
            var deferred = $q.defer();

            socket = io.connect();

            socket.on('connect', function () {
                console.log("connection success ...");
                deferred.resolve();
            });

            socket.on('connect_error', function () {
                console.log("connection error ...");
                deferred.reject();
            });

            socket.on('data', function (data) {
                if (!renderer.isRendering) {
                    renderer.startRendering();
                }

                graph.insertDataValue(data, "curve-1");
            });

            return deferred.promise;
        };

        MainService.setupRenderer = function () {
            renderer = new Renderer({
                htmlElementId: "chart-canvas"
            });

            renderer.init();
        };

        MainService.setupGraph = function () {
            graph = new Graph({
                id: "graph-1",
                renderer: renderer,
                color: Color.BLACK,
                xAxisMode: Graph.AxisMode.DATETIME,
                //data: data,
                //xAxisMode: Graph.AxisMode.DECIMAL,
                yAxisMode: Graph.AxisMode.DECIMAL,
                xAxisLabel: "Time",
                yAxisLabel: "Hook Load"
            });

            graph.init();
            renderer.addComponent(graph);
        };

        MainService.resumeStream = function () {
            if (!socket || !socket.connected) return;
            socket.emit('resume');
        };

        MainService.pauseStream = function () {
            if (!socket || !socket.connected) return;
            socket.emit('pause');
        };

        MainService.zoomIn = function () {
            graph.zoomPan.zoomIn();
        };

        MainService.zoomOut = function () {
            graph.zoomPan.zoomOut();
        };

        MainService.zoomReset = function () {
            graph.zoomPan.zoomReset();
        };

        MainService.autoScale = function () {
            graph.autoScale = true;
            graph.adjustScale();
        };

        return MainService;
    }]);

})(
    window,
    window.angular,
    io,
    window.angular.module("app")
);